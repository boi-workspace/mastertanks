MasterTanks change log:

IN PROGRESS:
- make one class very user-friendly - add javadoc for it
- map editor

TODO:
	big bois:
game over screen
truncate data to improve efficiency - remove double wherever possible
write readme and add instructions to gui
rewrite multiplayer
	client 'no host found' alert
	cut wifi mid at different points
	send respawn flashing data
	display port number on master page, do we need the port number?
	can we transfer more accurate positions? (currently only int)
	send scores
	remove frame-rate definition in slave -> adjustable frame rate on master end
 	finally {
            listener.close();
        }
	get ip from text field
make rotation incriments changeable -- see direction variable in tankSprite
highscore screen and save file
espape pause menu
ajustible window?
create .jar without intellij (remember how original jar doesn't even run??)
clean up artificial intelligence class
ai isn't impressive when it doesn't do anything. if player isn't moving it should at least do something...
option for defining controls
	quick-fixes:
grunt louder for computer
change powerup timing to be consistent, make it a rotation so all powerups are used in a game
beeping on countdown
record bleh sound effect (when it's quiet)
change title for master/slave (on top of window)
do we need the log file?
spawning inside heart
pause manu on escape and p
client freezes if invalid ip is tried, should say "attempting connection..."
convert all stage references to config.mainStage
be able to select and copy IP
power up always spawns in front of you at start of tutorial. power up spaws outside of bounds
longer buffs and shorter debuffs
bounds values for different versions of windows...
make it so you cant be killed by your own bullets
whats up with the garbage dump?
clean up config
fix high score screen - add button
multiplayer references to config - may not have the same config values on both ends (e.g. time limit)
after zombies config.map should reset (confusing if empty)

DONE:
- add animation behind master tanks title -- 900x200
- make flame animation for difficulties
	difficulty option is very bland, have larger flame for hard
-------------- ^RECENT^
- menu start up
	make start-up song
is it possible to chuck a processing applet in javafx? test applet embedding - A: NOPE
- remove game modes
- music
	convert songs to wav
	make player class
A* path planning
	difficulty option
	make tank drive towards target in shootLogicTrackDown
	smoothen path - target shouldn't just be the next node
	PROBLEM - ai rewards inactivity - should be the opposite. hunt down player but attack slowly
	merge with tank and remove old AI
	advanced shoot logic with math.random integration
	basic shoot logic
	line of sight funtion
	goTo function works with target
A* path planning
	code hits a wall with too many [closed?] nodes - exponential processing time after a certain number]
	I THINK IT'S STUCK IN A LOOP OF EVALUATING FUCKING CLOSED NODES - NUMBERS ARE FLICKERING
	^all fixed baby
test online with another computer
remove port number in whats my ip
controls panel in options
handle mid-game disconnection - include error message in menu
put the in-game socket code in the sceduled service
animate when turning
tank animation in slave
Cancal host works. added a timeout and refresh in the thread
	impliment ScheduledService
convert config to interface
sprite and run are now abstract
A* path planning
	all works with aiBoi sprite
	calculate H
	get directions set up and test them
	create and remove nodes with pointers to adjacents
	can create nodes and find the nearest one to a tank
compile a jar
put the music back on
re-design maps for new screen-size and screenshot for options
more robust animation framework in tankSprite
improve panda sprites
turn main menu into fxml file
	update colours to be softer on the eyes
turn options into fxml file
	make animations
panda shoot animation
changed power-up colours
power-up doesn't spawn in front of you at the start of every game
rewrite multiplayer
	cancel hosting
	get bullets working
	try onlineDataTransfer with no bullets
	slave - host tank vanishes after countdown - fixed
	remove power-up sprite
	get master working with same data transfer ( onlineDataTransfer() empty for both sides )
	slave quit works
	slave send works
	get slave running with data sent from master simulation
	how to handle bullets
rolling wheels animation
fix other game-modes after run update
use an fxml file for the menu layouts (make new javafx project and see template...)
	run superclass made
	get runSlave rendering with no input (static variables)
	establish socket connection and send message
	get ip address function for join game
	what is the time differnce between sending bits vs characters?
rewritten bulletHandling
cleaned up gameRun - new functions.
	swap options and tank builder button
	set up multiplayer scene in new class
	add multiplayer menu
	add buttons and formatting to scene
	understand packaging https://python-packaging-user-guide.readthedocs.io/tutorials/packaging-projects/
prevent window resizing
60fps /test config file
fix the god damn collision detection: (see COLLISION_DETECTION_NOTES.txt)
	boundary cd
	wall cd - overlap and mtv functionality
then fix up push out of walls
overlap function
bounds collision detection fixed.
new out of bounds values are good (for windows 10 at least)
get intersect and push into bounds stage working first
fixed outOfBounds function
music doesnt stop when game is started
turn test2 shitty class into a legit one
write another class for test2
can both be converted?
integrate into intellij
make jar of working game
learn how packages work
make packages

	--------------------------------------------------

LEARNING NOTES:

Power-ups:
much faster and mellee attack

Whenever you want to upload an unpresentable build, chuck it on a new branch. master is ONLY for stable pushs

** a static variable is one that would have a common value across a multitude of instances
** rather than having multiple instances of the same variable in memory,
** the variable will only be assigned memory once and will be shared by all instances
** note that a static method can be invoked without an instance of the class and is able to modify a static variable
** hence the static keyword is a memory saving technique
TODO find all instances of the static keyword and see if they match the use case

ART IDEAS:
tanks idea a lil lame - I want something fun!
power-ups aren't fun. I don't like the risk v reward concept, it's just the overwatch coin toss
it's just: pray to rngesus -> win or lose. thus all power-ups should be something worth chasing them
down for.

AI:
http://www.jgallant.com/nodal-pathfinding-in-unity-2d-with-a-in-non-grid-based-games/

Poced Gen:
https://gamedev.stackexchange.com/questions/79049/generating-tile-map

Threading??
https://stackoverflow.com/questions/36358062/is-using-timeline-for-scheduling-correct
java ScheduledService example

change ico:
https://www.howtogeek.com/75983/stupid-geek-tricks-how-to-modify-the-icon-of-an-.exe-file/

compile:
https://www.sergiy.ca/how-to-compile-and-launch-java-code-from-command-line/

ogg player
https://github.com/nnttoo/OggJavaPlayer
https://stackoverflow.com/questions/24764960/how-to-play-ogg-files-in-java
https://gamedev.stackexchange.com/questions/3672/how-to-loop-over-a-part-of-an-ogg-vorbis-stream
https://books.google.co.uk/books?id=ka2VUBqHiWkC&pg=PA55&lpg=PA55&dq=effective+java+clone&source=bl&ots=yXGhLnv4O4&sig=zvEip5tp5KGgwqO1sCWgtGyJ1Ns&hl=en&ei=CYANSqygK8jktgfM-JGcCA&sa=X&oi=book_result&ct=result&redir_esc=y#v=onepage&q=effective%20java%20clone&f=false

processing
https://stackoverflow.com/questions/28266274/how-to-embed-a-papplet-in-javafx#28278885
https://stackoverflow.com/questions/27948634/how-do-i-use-a-processing-code-in-javafx
https://stackoverflow.com/questions/23662895/is-it-possible-to-use-an-existing-applet-within-a-javafx-application-desktop
****https://happycoding.io/tutorials/java/processing-in-java