﻿## Master Tanks

---

**About the Project**

In 2016 Ethan Yee and myself created a simple tank game for a university project. The code in this repository started with a half-finished version of this project as a starting framework, and excludes a number of features such as the buggy collision detection and all of the python networking code. I have cleaned, rewritten and added code to this project to showcase my **java** programming ability and understanding of **object-oriented** design.

**About the Game**

The application uses javafx and I started using java 10 in the intellij ide, but have now upgraded to java 11 and use **gradle** for building and packaging (the javafx libraries are included in the jar file). To run it:

1. All platforms
	- Install the latest version of the java runtime environment (JRE) or JRD at https://www.oracle.com/technetwork/java/javase/downloads/index.html
	- Download the latest version of MasterTanks.jar from the downloads page in this repository
2. Windows
	- Alternatively, if you're on windows you can download the executable on the downloads page which won't give you a virus I swear!
3. Run it baby!

I have only tested it on windows 10 so you have my apologies if the program messes up on another os. Nearly all of the content that was written by my project partner Ethan has been overwritten so you can judge me by the state of the game and code.

Game Controls
Move - W,A,S,D
Shoot - SPACE
Stop Game - ESC

To test the multiplayer, run 2 instances of the game. Host the game with one of them and enter the IP as a client for the other. If you're running both on the same machine, typing 'localhost' will do the trick on the client side.

**Features**

- *Collision detection logic* - DONE - Used the separating axis theorem and a lot of vector calculus.
- *Online multiplayer* - DONE - Peer-to-peer system with sockets (to test run two instances of the code, the first hosting and the second connecting as a client)
- *Menus and interface* - DONE - I also used xml and css files to design them. All art is original.
- *AI* - DONE - A star is used for the path planning and a random element is involved once the AI is in shooting range. Implemented **multithreading** for the A star processing as well as the online communication.
- *Music and Art and Stuff* - All original m8.
- *Procedurally generated maps* - TODO
- *Graphical and music processing* - NOT ME - All handled by javafx libraries.
