package csmasters.multiplayer;

import csmasters.runtime.runMaster;
import csmasters.setup.masterTanks;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;

public class mpControllerHost {

    ////////////////////////// MASTER - HOST GAME

    @FXML
    public void gotoMultiplayer() {  masterTanks.getMultiplayer().setMultiplayerScene(); }

    @FXML
    public Label ipDisplay;
    @FXML
    public Label statusDisplayHost;
    public Button hostButton;
    public Button ipButton;
    public Button cancelButton;

    private Socket hostSocket;
    private ServerSocket listener;
    private boolean isListening = false;
    mpHostListener hostThread;

    @FXML
    public void hostServer() {
        statusDisplayHost.setText("Status: Hosting! waiting for\nchallenger...");
        try {
            //socketServer();
            listener = new ServerSocket(masterTanks.getMultiplayer().myPort);
            listener.setSoTimeout(1000); // IMPORTANT set timeout in ms
            hostThread = new mpHostListener(listener);
            hostThread.start(); // starts a server in another thread that listens for a client
            isListening = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private class mpHostListener extends Thread {
        // http://cs.lmu.edu/~ray/notes/javanetexamples/
        ServerSocket listener;

        mpHostListener(ServerSocket listener) { this.listener = listener; }

        @Override public void run() {
            Boolean loop = true;
            while (loop) {
                System.out.println("attempt host connection...");
                loop = open();
                if (this.isInterrupted()) {
                    loop = false; // escape
                    Platform.runLater(() -> closeSocket()); // ensures closeSocket() is run in the javaFX thread
                }
            }
        }

        private Boolean open() {
            // returns true to try run this again
            try {
                hostSocket = listener.accept(); // waits here until a client connects
                Platform.runLater(() -> { // ensures runHost() is run in the javaFX thread
                    try {
                        runHost();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }});
                return false; // success: escape loop
            } catch (SocketTimeoutException e) {
                return true; // timeout: stay in loop
            } catch (IOException e) {
                e.printStackTrace();
                this.interrupt();
                return false; // error: escape loop
            }
        }
    }
    private void runHost() throws IOException {

        BufferedReader inReader = new BufferedReader(new InputStreamReader(hostSocket.getInputStream()));
        PrintWriter outWriter = new PrintWriter(hostSocket.getOutputStream(), true);
        // write
        outWriter.println("Connection established!");

        statusDisplayHost.setText("Status: Game finished!");
        runMaster game = new runMaster(hostSocket, listener, inReader, outWriter, this);
        game.gameStart(masterTanks.getStage());

        // note that the socket isn't closed until Quit() is called in runMaster
        System.out.println("Host done.");
    }
    @FXML
    public void getIP() {
        try {
            masterTanks.getMultiplayer().myIP = Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            masterTanks.getMultiplayer().myIP = "unknown";
        }
        ipDisplay.setText("IP address: "+masterTanks.getMultiplayer().myIP);
    }
    @FXML
    public void cancelHost() {
        if (isListening) {
            hostThread.interrupt(); // closeSocket is called after the thread shuts down
            isListening = false;
        }
        statusDisplayHost.setText("Status: Idle");
        gotoMultiplayer();
    }
    private void closeSocket() {
        // called to close a pending connection
        try {
            listener.close();
            // note that hostSocket is null if connection is not sucessful, so it doesn't need to be closed here
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setStatusDisplay(String status) {
        statusDisplayHost.setText(status);
    }
}
