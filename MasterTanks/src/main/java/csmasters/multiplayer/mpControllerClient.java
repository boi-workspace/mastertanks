package csmasters.multiplayer;

import csmasters.runtime.runSlave;
import csmasters.setup.masterTanks;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Window;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class mpControllerClient {

////////////////////////// SLAVE - JOIN GAME

    @FXML
    public void gotoMultiplayer() {  masterTanks.getMultiplayer().setMultiplayerScene(); }

    @FXML
    public Label statusDisplayClient;
    @FXML
    public TextField enterIP;
    @FXML
    public Button connectButton;

    @FXML
    public void connect() {
        statusDisplayClient.setText("Status: Atempting connection...");
        try {
            runClient();
        } catch (IOException e) {
            e.printStackTrace();
            statusDisplayClient.setText("Status: Failed Connection!");
        }
    }
    private void runClient() throws IOException {
        statusDisplayClient.setText("Status: Connecting...");
        Window owner = connectButton.getScene().getWindow();
        if(enterIP.getText().isEmpty()) {
            alertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form Error",
                    "Please an IP address");
            return;
        }

        // TODO: run in another thread?
        // confirm connection TODO: error handling 'No connection' java.net.ConnectException: Connection refused: connect
        Socket socket = new Socket(enterIP.getText(), masterTanks.getMultiplayer().pairPort); // attempt address connection

        BufferedReader inReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter outWriter = new PrintWriter(socket.getOutputStream(), true);
        // read
        String inputString;
        inputString = inReader.readLine();
        System.out.println(inputString); // Connection established!

        statusDisplayClient.setText("Status: Game finished!");
        runSlave game = new runSlave(socket, inReader, outWriter, this);
        game.gameStart(masterTanks.getStage());

        // note that the socket isn't closed until Quit() is called in runSlave
        System.out.println("Client done.");
    }
    public void setStatusDisplay(String status) {
        statusDisplayClient.setText(status);
    }
}
