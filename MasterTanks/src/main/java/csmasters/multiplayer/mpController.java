package csmasters.multiplayer;

import csmasters.setup.masterTanks;
import javafx.fxml.FXML;

public class mpController {
    @FXML
    public void gotoMain() {  masterTanks.getMultiplayer().setMainScene(); }
    @FXML
    public void gotoMaster() { masterTanks.getMultiplayer().setHostScene(); }
    @FXML
    public void gotoSlave() { masterTanks.getMultiplayer().setClientScene(); }
}
