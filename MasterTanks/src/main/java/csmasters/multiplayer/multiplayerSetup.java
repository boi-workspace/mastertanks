package csmasters.multiplayer;

import csmasters.setup.config;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.IOException;

public class multiplayerSetup {

    private Scene startScene, multiplayerScene, hostScene, clientScene;
    private Stage mainStage;

    // multiplayer data
    String myIP = "unknown";
    int myPort = 9090;
    int pairPort = 9090;

    public multiplayerSetup(Stage mainStage, Scene startScene) {
        this.mainStage = mainStage;
        this.startScene = startScene;

        try {
            Parent root = FXMLLoader.load(getClass().getResource( config.mpURL ));
            multiplayerScene = new Scene(root);
            root = FXMLLoader.load(getClass().getResource( config.mpHostURL ));
            hostScene = new Scene(root);
            root = FXMLLoader.load(getClass().getResource( config.mpClientURL ));
            clientScene = new Scene(root);
        } catch(IOException ie) {
            ie.printStackTrace();
            BorderPane multiplayerLayout = new BorderPane();
            multiplayerLayout.setStyle("-fx-background-color:   linear-gradient(#ffff00,    #ff4500); -fx-padding:20px;");
            multiplayerScene = new Scene(multiplayerLayout);
            System.out.println("error");
        }
    }

    public void setMainScene() { mainStage.setScene(startScene); }
    public void setMultiplayerScene() { mainStage.setScene(multiplayerScene); }
    public void setHostScene() { mainStage.setScene(hostScene); }
    public void setClientScene() { mainStage.setScene(clientScene); }
}
