/*
Authors: David Hamill and Ethan Yee

This class runs all gameplay operations along with a host of other classes.
 */
package csmasters.runtime;

import csmasters.setup.masterTanks;
import csmasters.setup.musicPlayer;

import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import javafx.animation.Timeline;
import javafx.geometry.VPos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import java.sql.Timestamp;
import java.util.Date;

public class runOffline extends run {

    private boolean isPaused = false;
    private String[] tank2Controls;
	private static aiAStar AI;
	private static int[] aiTarget = {0,0};
	private Boolean pathFound = true;
	private aiBoi boi;

////////////////////////////////////GAME LOOP

    protected void gameProcessing() {
		time++;
		if (time < 0) { // pre masterTanks

			// rendering
			gc.clearRect(0, 0, 1024, 768);
			renderSprites();
			renderCountdown();

		} else if (time < gameTime) { // in game

			gc.clearRect(0, 0, 1024, 768);

			// object processing
			tankProcessing();
			bulletProcessing();
			powerUp.powerUpGen(walls);

			// rendering
			renderScore();
			renderSprites();
			renderCountdown();

		} else { // time up
			Quit();
		}
	}

////////////////////////////////////PARALLEL LOOP

	protected void startParallelLoop() {
		processingLoop = new parallelAI();
		processingLoop.setPeriod(Duration.seconds(secondsPerFrame));
		processingLoop.setOnFailed(t -> Quit());
		processingLoop.start();
	}
	private class parallelAI extends parallel {
		@Override protected void parallelProcessing() {
			//System.out.println("run ai "+time); System.out.flush();
			if (aiTesting)
				pathFound = AI.process(boi, tank1);
			else if (masterTanks.getGameMode() == 1) // single player
				pathFound = AI.process(tank2, tank1);

			if (pathFound) {
				aiTarget = AI.targetSmoothed();
			}
		}
	}

////////////////////////////////////RENDERING

	@Override protected void renderSprites() {
		tank1.render(gc);
		tank2.render(gc);
		for (spriteWall wall : walls){ wall.render(gc); }
		for (spriteBullet bullet : bullets){ bullet.render(gc);}
		if(powerUp.getOperational())
			powerUp.render(gc);

		if (aiTesting)
			boi.render(gc);
		if (debuggingMode)
			AI.renderNodes(gc);
	}

////////////////////////////////////SPRITE INTERACTIONS

	private void tankProcessing() {
		updateTankResponse(tank1, tank1Controls); // player 1
		tankUpdate(tank1, true);

		// Game mode (training - 0, single player - 1, local multiplayer - 2)
		// SP Mode (normal oldAI - 0, 1v3 - 1, survival - 2)

		if (masterTanks.getGameMode()  == 2) { // local multiplayer
			updateTankResponse(tank2, tank2Controls);
			tankUpdate(tank2, false);
		} else if (masterTanks.getGameMode()  == 1) { // single player
			aiBrain.tankAI(tank2,tank1,aiTarget,walls,time);
			tankUpdate(tank2, false);
		} // do nothing for training

		if (aiTesting){
			boi.updatePosition(aiTarget);
		}
	}
	private void tankUpdate(spriteTank tank, boolean isTank1) { // isTank1 true if tank == tank1
		tank.updateVelocity();
		tank.updatePosition(walls);
		if(powerUp.isRetrievedBy(tank)) {
			tank.powerUp();
			powerUp.setOperational();
		}
		// tanksCollide only needs to be called by tank1
		if(isTank1) {
			boolean collide;
			collide = tank.tanksCollide(walls, tank2, gc);
			if (collide) {
				tank1Score++;
				tank2Score++;
			}
		}
	}

	private void bulletProcessing() {
        //Collisions with bullets
        int i = 0;
        spriteBullet bullet;
        while (i < bullets.size()) {
			bullet = bullets.get(i);
        	bullet.updatePosition(walls);
        	if (tank1.isShotBy(bullet)) {
				tank2Score = bulletHitTank(tank1, tank2, tank2Score);
        		bullets.remove(i);
        	} else if (tank2.isShotBy(bullet)) {
				tank1Score = bulletHitTank(tank2, tank1, tank1Score);
				bullets.remove(i);
			} else if (bullet.counter()) {
            	bullets.remove(i);
        	} else {
            	i++;
        	}
        }
    }
    private int bulletHitTank(spriteTank tank, spriteTank otherTank, int otherTankScore) {
		if (tank.bubbleShield) {
			tank.powerDown();
		} else {
			otherTankScore++;
			tank.respawn(walls, otherTank, gc);
		}
		return otherTankScore;
	}

////////////////////////////////////INPUT HANDLING

    protected void prepareActionHandlers() { // this describes what to do when certain keyboard events occur
    	gameScene.setOnKeyPressed(event -> { // when a key is released...
			if (event.getCode().toString().equals("ESCAPE")) {
				Quit();
			}
			if (!activeKeys.contains(event.getCode().toString())) {
				// write to log file
				Date date= new Date();
				logFile.printf("%-25s %s%n", new Timestamp(date.getTime()), event.getCode().toString());
				activeKeys.add(event.getCode().toString()); // it is added to the list
				if (event.getCode().toString().equals("P")) {
					if (isPaused) {
						gameLoop.play();
						isPaused = false;
					} else {
						gameLoop.pause();
						isPaused = true;
						gc.setTextAlign(TextAlignment.CENTER);
						gc.setTextBaseline(VPos.CENTER);
						gc.setFill(Color.YELLOW);
						gc.setFont(masterTanks.getButtonFont());

						gc.fillText(
								"** PAUSED **",
								Math.round(canvas.getWidth() / 2),
								Math.round(canvas.getHeight() / 2)
							);
					}
				}
			}
		});

    	gameScene.setOnKeyReleased(event -> { // when a key is released...
			activeKeys.remove(event.getCode().toString()); // it is added to the list
		});
    }
	private void updateTankResponse(spriteTank tank, String[] tankControls) {
		if (activeKeys.contains(tankControls[1])^activeKeys.contains(tankControls[3])) {
			tank.incrementDirection(activeKeys.contains(tankControls[3]), time); // rotate clockwise
		}

		tank.setMovement(activeKeys.contains(tankControls[0])^activeKeys.contains(tankControls[2]),
				activeKeys.contains(tankControls[0]));

		if (activeKeys.contains(tankControls[4])) {
			tank.shoot(time);
		}
	}

///////////////////////////////////INITIALIZE SCENE AND VARIABLES

    @Override protected void prepareGame(Stage mainStage) {
    	activeKeys = new HashSet<>(); // contains the names of the keys currently pressed
        // use a HashSet so duplicates are not possible

      	canvas = new Canvas( xCanvasSize, yCanvasSize ); // an image on which we can draw stuff such as text and images
        gc = canvas.getGraphicsContext2D(); // we'll use this GraphicsContext object to draw stuff on the canvas

    	// creates a background image on which the canvas is drawn.
        StackPane stack = new StackPane();
        stack.setMaxSize(canvas.getWidth(), canvas.getHeight());
		String url = getClass().getResource( gameBackgroundURL ).toExternalForm();
		stack.setStyle("-fx-background-image: url('" + url + "');");
        stack.getChildren().add(canvas);

        gameLoop.setCycleCount( Timeline.INDEFINITE );

		try {
			logFile = new PrintWriter("logFile.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

    	//Preparing the scene and
    	this.mainStage = mainStage;
    	gameScene = new Scene( stack ); // background stack is our root

    	if (masterTanks.getGameMode() == 2) {
    		player2name = "Player 2";
    	} else {
			player2name = "Computer";
    	}

		// play the game song
		if (enableMusic)
			musicPlayer.play("game");
    }
    protected void createGameObjects() {
		tank1 = new spriteTank(50, 340, masterTanks.getTank(), 4);
		tank1Controls = new String[] {"W", "A", "S", "D", "SPACE"};
		tank1.loadBullets(bullets);

		if (masterTanks.getGameMode()  == 2) {
			tank2 = new spriteTank(900, 340, 4, 12);
			tank2Controls = new String[] {"NUMPAD8", "NUMPAD4", "NUMPAD5", "NUMPAD6", "NUMPAD0"};
			tank2.loadBullets(bullets);
		} else if (masterTanks.getGameMode()  == 1) {
			tank2 = new spriteTank(900, 340, 4, 12);
			tank2.loadBullets(bullets);
		} else if (masterTanks.getGameMode()  == 0) {
			tank2 = new spriteTank(900, 340, 5, 0);
		}

		powerUp = new spritePowerUp();

		if (masterTanks.getMap() == 0) {
			// no map
		} else if (masterTanks.getMap() == 1) {
			makeWalls1();
		} else if (masterTanks.getMap() == 2) {
			makeWalls2();
		}

		if (aiTesting){
			boi = new aiBoi();
			aiTarget[0] = (int)boi.getX();
			aiTarget[1] = (int)boi.getY();
		}
		AI = new aiAStar(walls, tank1.getSize());
	}

///////////////////////////////////UNUSED FUNCTIONS
	protected void onlineSynch() {} // must be overriden to satisfy abstract inherritance of run
}

