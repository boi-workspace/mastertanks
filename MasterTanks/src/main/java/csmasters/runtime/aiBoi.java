/*
This class is for testing my new ai!
enable aiTesting in config to see this puppy in action
 */

package csmasters.runtime;

import csmasters.setup.config;
import javafx.scene.image.Image;

class aiBoi extends sprite {
    private double speed = 2;

    aiBoi() {
        image = new Image("/assets/boi.png");
        width = image.getWidth(); // for collision detection
        height = image.getHeight();
        positionX = Math.random()*(config.xCanvasSize-width);
        positionY = Math.random()*(config.yCanvasSize-height);
        xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
        numVertices = 4;
    }

    void updatePosition(int[] target) {
        double vx = target[0] - positionX-width/2; // vector from this to target
        double vy = target[1] - positionY-height/2;
        double magnitude = Math.sqrt(vx*vx + vy*vy); // magnitude of vector
        if (magnitude > 1) {
            vx *= speed / magnitude; vy *= speed / magnitude; // convert to unit vector and multiply by speed
            positionX += vx; positionY += vy;
        }
    }
}
