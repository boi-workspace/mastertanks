/*
Used by aiBrain to create a polygon inbetween 2 tanks and uses intersects()
to detect if any walls are within this polygon
 */

package csmasters.runtime;

class aiLineOfSightPolygon extends sprite {
	aiLineOfSightPolygon(double[] xVertices, double[] yVertices){
		this.xVertices = xVertices;
		this.yVertices = yVertices;
	}
}
