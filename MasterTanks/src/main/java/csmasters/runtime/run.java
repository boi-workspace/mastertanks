package csmasters.runtime;

import csmasters.setup.config;
import csmasters.setup.masterTanks;
import csmasters.setup.musicPlayer;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public abstract class run implements config {

    Stage mainStage;
    Scene gameScene;

////////////////////////////////////DATA

    Canvas canvas;
    GraphicsContext gc;
    Timeline gameLoop = new Timeline();
    parallel processingLoop;
    int time = -90;
    PrintWriter logFile;
    int tank1Score = 0;
    int tank2Score = 0;
    HashSet<String> activeKeys; // i'll use this to store the keys that have been pressed
    String[] tank1Controls;
    String player2name = "Player 2"; // may be renamed to computer, see runOffline -> prepareGame()

////////////////////////////////////OBJECTS

    spriteTank tank1;
    spriteTank tank2;
    ArrayList<spriteBullet> bullets = new ArrayList<>();
    ArrayList<spriteWall> walls = new ArrayList<>();
    spritePowerUp powerUp;

    public void gameStart(Stage mainStage) {

        prepareGame(mainStage);
        prepareActionHandlers();
        createGameObjects();

        try {
            onlineSynch(); // wait until both master and slave are ready
        } catch(Exception e) {
            e.printStackTrace();
        }

        ////////////////////////// FIXED LOOP

        KeyFrame kf = new KeyFrame(
                Duration.seconds(secondsPerFrame),
                ae -> { // event handler
                    gameProcessing();
                });

        gameLoop.setCycleCount( Timeline.INDEFINITE );
        gameLoop.getKeyFrames().add( kf );
        gameLoop.play();

        ////////////////////////// FLEXABLE LOOP

        startParallelLoop();

        ////////////////////////// SET SCENE

        mainStage.setOnCloseRequest(we -> logFile.close());
        mainStage.setScene(gameScene);
        mainStage.show();
    }

////////////////////////////////////INITIALIZING

    protected void prepareGame(Stage mainStage) {
        activeKeys = new HashSet<>(); // see prepareActionHandlers

        canvas = new Canvas( xCanvasSize, yCanvasSize );
        gc = canvas.getGraphicsContext2D();

        // creates a background image on which the canvas is drawn.
        StackPane stack = new StackPane();
        stack.setMaxSize(canvas.getWidth(), canvas.getHeight());
        String url = getClass().getResource( gameBackgroundURL ).toExternalForm();
        stack.setStyle("-fx-background-image: url('" + url + "');");
        stack.getChildren().add(canvas);

        try {
            logFile = new PrintWriter("logFile.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // play the game song
        if (enableMusic)
            musicPlayer.play("game");

        //Preparing the scene and
        this.mainStage = mainStage;
        gameScene = new Scene( stack ); // background stack is our root
    }
    void makeWalls1() {
        int startX, startY;
        int xOffset = 150;
        int yOffset = 100;

        // top left
        startX = xOffset;
        startY = yOffset;
        for (int i = 0; i < 13; i++){
            walls.add(new spriteWall(startX+144-(i)*12,startY,i%2+1));
        } for (int i = 13; i < 20; i++){
            walls.add(new spriteWall(startX,startY+12+(i-13)*12,i%2+1));
        }

        // top right
        startX = xCanvasSize - xOffset;
        startY = yOffset;
        for (int i = 20; i < 33; i++){
            walls.add(new spriteWall(startX-144+(i-20)*12,startY,i%2+1));
        } for (int i = 33; i < 40; i++){
            walls.add(new spriteWall(startX,startY+12+(i-33)*12,i%2+1));
        }

        // bottom left
        startX = xOffset;
        startY = yCanvasSize - yOffset;
        for (int i = 40; i < 53; i++){
            walls.add(new spriteWall(startX+144-(i-40)*12,startY,i%2+1));
        } for (int i = 53; i < 60; i++){
            walls.add(new spriteWall(startX,startY-12-(i-53)*12,i%2+1));
        }

        // bottom right
        startX = xCanvasSize - xOffset;
        startY = yCanvasSize - yOffset;
        for (int i = 60; i < 73; i++){
            walls.add(new spriteWall(startX-144+(i-60)*12,startY,i%2+1));
        } for (int i = 73; i < 80; i++){
            walls.add(new spriteWall(startX,startY-12-(i-73)*12,i%2+1));
        }

        // <3
        int cx = xCanvasSize/2;
        int cy = yCanvasSize/2;
        int s = 12;

        walls.add(new spriteWall(cx,cy-4*s,1));
        walls.add(new spriteWall(cx+s,cy-5*s,1));
        walls.add(new spriteWall(cx+2*s,cy-5*s,1));
        walls.add(new spriteWall(cx+3*s,cy-4*s,1));
        walls.add(new spriteWall(cx+4*s,cy-3*s,1));
        walls.add(new spriteWall(cx+4*s,cy-2*s,1));
        walls.add(new spriteWall(cx+4*s,cy-s,1));
        walls.add(new spriteWall(cx+4*s,cy,1));
        walls.add(new spriteWall(cx+3*s,cy+s,1));
        walls.add(new spriteWall(cx+2*s,cy+2*s,1));
        walls.add(new spriteWall(cx+s,cy+3*s,1));

        walls.add(new spriteWall(cx-s,cy-5*s,1));
        walls.add(new spriteWall(cx-2*s,cy-5*s,1));
        walls.add(new spriteWall(cx-3*s,cy-4*s,1));
        walls.add(new spriteWall(cx-4*s,cy-3*s,1));
        walls.add(new spriteWall(cx-4*s,cy-2*s,1));
        walls.add(new spriteWall(cx-4*s,cy-s,1));
        walls.add(new spriteWall(cx-4*s,cy,1));
        walls.add(new spriteWall(cx-3*s,cy+s,1));
        walls.add(new spriteWall(cx-2*s,cy+2*s,1));
        walls.add(new spriteWall(cx-s,cy+3*s,1));
        walls.add(new spriteWall(cx,cy+4*s,1));
    }
    void makeWalls2() {

        for (int i = 0; i < 56; i++){
            walls.add(new spriteWall(180+i*12,270,i%2+1));
        }
        for (int i = 0; i < 12; i++){
            walls.add(new spriteWall(180+46*12,270-(i+1)*12,i%2+1));
        } // 68

        for (int i = 0; i < 56; i++){
            walls.add(new spriteWall(180+i*12,yCanvasSize-270,i%2+1));
        }
        for (int i = 0; i < 12; i++){
            walls.add(new spriteWall(180+10*12,yCanvasSize-270+(i+1)*12,i%2+1));
        } // 136

        for (int i = 0; i < 12; i++){
            walls.add(new spriteWall(180+10*12,i*12,i%2+1));
        } // 148

        for (int i = 0; i < 12; i++){
            walls.add(new spriteWall(180+46*12,yCanvasSize-i*12,i%2+1));
        } // 160
    }

////////////////////////////////////RENDERING

    void renderSprites() {
        tank1.render(gc);
        tank2.render(gc);
        if(powerUp.getOperational())
            powerUp.render(gc);
        for (spriteWall wall : walls){ wall.render(gc); }
        for (spriteBullet bullet : bullets){ bullet.render(gc);}
    }
    void renderScore() {
        // scoring
        gc.setTextAlign(TextAlignment.LEFT);
        gc.setTextBaseline(VPos.CENTER);
        gc.setFill(Color.YELLOW);
        gc.setFont(masterTanks.getTextFont());
        gc.fillText("Player 1" + " score: " + Integer.toString(tank1Score), 10, 15);
        gc.fillText(player2name + " score: " + Integer.toString(tank2Score), 10, 40);
        gc.fillText("Time remaining: " + Integer.toString((gameTime - time)/30 + 1) + " seconds", 10, 65);
    }
    void renderCountdown() {
        if (time < 0) {
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFill(Color.YELLOW);
            gc.setFont(masterTanks.getButtonFont());
            gc.fillText(Integer.toString(1 - time / 30),
                    Math.round(canvas.getWidth() / 2),
                    Math.round(canvas.getHeight() / 2 - 5));
            gc.fillText("READY!",
                    Math.round(canvas.getWidth() / 2),
                    Math.round(canvas.getHeight() / 2 - 90));
        }
        if (gameTime-time < 300) { // countdown
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFill(Color.YELLOW);
            gc.setFont(masterTanks.getButtonFont());
            gc.fillText( Integer.toString((gameTime - time)/30 + 1),
                    Math.round(canvas.getWidth()/2),
                    Math.round(canvas.getHeight()/2-5) );
        }
    }

////////////////////////////////////UNDEFINED FUNTIONS (DEFINED BY SUB-CLASSES)

    protected abstract void startParallelLoop();
    protected abstract void onlineSynch() throws IOException;
    protected abstract void prepareActionHandlers();
    protected abstract void createGameObjects();
    protected abstract void gameProcessing();

///////////////////////////////////QUIT

    protected void Quit() {
        // stop the game and processing loops
        processingLoop.cancel();
        gameLoop.stop();

        try { // TODO remove log file funtionality
            logFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // stop game music and masterTanks menu music
        if (enableMusic)
            musicPlayer.play("menu");

        // go to main menu
        mainStage.setScene(masterTanks.getStartScene());
    }
}
