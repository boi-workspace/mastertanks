package csmasters.runtime;

import csmasters.setup.config;
import csmasters.setup.masterTanks;
import csmasters.setup.musicPlayer;
import csmasters.multiplayer.mpControllerHost;

import javafx.application.Platform;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;

public class runMaster extends run {

    // Data
    private int[] hostTank = {50, 340, 4}; // [x,y,d]
    private int[] clientTank = {900, 340, 12}; // [x,y,d]
    private boolean quitKey = false; // true when we want to quit
    private boolean quit = false; // TODO double check functionality
    private boolean[] tank2KeyInputs = {false,false,false,false,false}; // [W, A, S, D, SPACE]

    // socket variables
    private Socket socket;
    private ServerSocket listener;
    private BufferedReader inReader;
    private PrintWriter outWriter;
    private String inputString;
    private mpControllerHost controller;

    public runMaster(Socket socket, ServerSocket listener,
                     BufferedReader inReader, PrintWriter outWriter,
                     mpControllerHost controller) {
        this.socket = socket;
        this.listener = listener;
        this.inReader = inReader;
        this.outWriter = outWriter;
        this.controller = controller;
    }

////////////////////////////////////GAME LOOP

    protected void gameProcessing() {
        time++;

        if (quit) {
            Quit();

        } else if (time < 0) { // pre masterTanks

            // rendering
            gc.clearRect(0, 0, 1024, 768);
            renderSprites();
            renderCountdown();

        } else if (time < config.gameTime) { // in game

            // object processing
            tankProcessing();
            bulletProcessing();

            // rendering
            gc.clearRect(0, 0, 1024, 768);
            renderScore();
            renderSprites();
            renderCountdown();

        } else { // time up
            quitKey = true;
        }
    }

////////////////////////////////////PARALLEL LOOP

    protected void startParallelLoop() {
        processingLoop = new parallelSocket();
        processingLoop.setPeriod(Duration.seconds(secondsPerFrame));
        processingLoop.setOnFailed(t -> Quit());
        processingLoop.start();
    }
    private class parallelSocket extends parallel {
        @Override protected void parallelProcessing() {
            System.out.println("run socket "+time); System.out.flush();

            // data transfer
            try {
                // send positions and time, receive keyboard inputs
                onlineDataTransfer();
            } catch (IOException e) {
                controller.setStatusDisplay("In-Game Error: Connection Broken");
                Platform.runLater(() -> Quit());
                e.printStackTrace();
            }
        }
    }

////////////////////////////////////INITIALIZING

    protected void createGameObjects() {
        masterTanks.getStage().setTitle( "Master Tanks Host" );

        tank1 = new spriteTank(hostTank[0], hostTank[1], masterTanks.getTank(), hostTank[2]);
        tank1Controls = new String[] {"W", "A", "S", "D", "SPACE"};
        tank1.loadBullets(bullets);

        tank2 = new spriteTank(clientTank[0], clientTank[1], 4, clientTank[2]);
        tank2.loadBullets(bullets);

        // not used but required to prevent rendering errors (see renderSprites in run)
        powerUp = new spritePowerUp();

        makeWalls1();
    }
    protected void onlineSynch() throws IOException {
        // The initial synchronization

        // write
        outWriter.println("Host ready!");

        // read
        inputString = inReader.readLine();
        System.out.println(inputString); // Client ready!

        sendData();
        recieveData();
    }

////////////////////////////////////ONLINE DATA TRANSFER

    private void onlineDataTransfer() throws IOException {
        // send positions and time, recieve keyboard inputs

        sendData();
        recieveData();
    }
    private void sendData() {
        /*
        String gameData = "Data\n" +
                "50,340,4,\n" +    // Host Tank [x,y,d]
                "900,340,12,\n" +  // Client Tank [x,y,d]
                "0\n" +           // Time
                "0,1,3,\n" +       // Bullet IDs
                "10,20,300,\n" +   // Bullet x positions
                "30,20,120,\n" +   // Bullet y positions
                "false";          // Quit = true or false
        */
        // write
        String gameData = "Data\n";     // Data

        for (int i = 0; i < 3; i++) {   // Host Tank [x,y,d]
            gameData += hostTank[i] + ",";
        } gameData += "\n";

        for (int i = 0; i < 3; i++) {   // Client Tank [x,y,d]
            gameData += clientTank[i] + ",";
        } gameData += "\n";

        gameData += time + "\n";        // Time

        if (bullets.size() == 0) {
            gameData += "null\n";           // Bullet x positions
            gameData += "null\n";           // Bullet y positions
        } else {
            int j = 0;
            spriteBullet bullet;
            while (j < bullets.size()) {
                bullet = bullets.get(j);
                gameData += (int)bullet.getX();
                gameData += ",";
                j++;
            }
            gameData += "\n";

            j = 0;
            while (j < bullets.size()) {
                bullet = bullets.get(j);
                gameData += (int)bullet.getY();
                gameData += ",";
                j++;
            }
            gameData += "\n";
        }

        if (quitKey) {                  // Quit = true or false
            gameData += "true";
            quit = true;
        } else {
            gameData += "false";
        }

        outWriter.println(gameData);
    }
    private void recieveData() throws IOException {
        /*
        String gameData = "Data\n" +
                "01001\n" +    // Player Inputs [W,A,S,D,SPACE]
                "false";       // Quit = true or false
        */
        inputString = inReader.readLine(); // Data
        if (!inputString.equals("Data")) { // should equal 'Data', otherwise an error has occured
            System.out.println("Error: Corrupted data recieved");
            //Quit(); TODO exception handelling?
        }

        inputString = inReader.readLine(); // W A S D SPACE
        for (int i = 0; i < 5; i++) {
            tank2KeyInputs[i] = (inputString.charAt(i) == '1');
        }

        inputString = inReader.readLine(); // Quit = true or false
        if (inputString.equals("true"))
            quit = true;
    }

////////////////////////////////////SPRITE PROCESSING

    private void tankProcessing() {
        updateMasterTankResponse();
        tank1.updateVelocity();
        tank1.updatePosition(walls);

        updateSlaveTankResponse();
        tank2.updateVelocity();
        tank2.updatePosition(walls);

        if (tank1.tanksCollide(walls, tank2, gc)) { // respawns tanks if they collide
            tank1Score++;
            tank2Score++;
        }

        hostTank[0] = (int)tank1.getX();
        hostTank[1] = (int)tank1.getY();
        hostTank[2] = tank1.getDirection();
        clientTank[0] = (int)tank2.getX();
        clientTank[1] = (int)tank2.getY();
        clientTank[2] = tank2.getDirection();
    }
    private void bulletProcessing() {
        //Collisions with bullets
        int i = 0;
        spriteBullet bullet;
        while (i < bullets.size()) {
            bullet = bullets.get(i);
            bullet.updatePosition(walls);
            if (tank1.isShotBy(bullet)) {
                tank1.respawn(walls, tank2, gc);
                tank2Score++;
                bullets.remove(i);
            } else if (tank2.isShotBy(bullet)) {
                tank2.respawn(walls, tank1, gc);
                tank1Score++;
                bullets.remove(i);
            } else if (bullet.counter()) {
                bullets.remove(i);
            } else {
                i++;
            }
        }
    }

////////////////////////////////////INPUT HANDLING

    protected void prepareActionHandlers() {
        gameScene.setOnKeyPressed(event -> { // when a key is released...
            if (event.getCode().toString().equals("ESCAPE")) {
                quitKey = true;
            }
            if (!activeKeys.contains(event.getCode().toString())) {
                // write to log file
                Date date= new Date();
                logFile.printf("%-25s %s%n", new Timestamp(date.getTime()), event.getCode().toString());
                activeKeys.add(event.getCode().toString()); // it is added to the list
            }
        });
        gameScene.setOnKeyReleased(event -> { // when a key is released...
            activeKeys.remove(event.getCode().toString()); // it is added to the list
        });
    }
    private void updateMasterTankResponse() {
        // tank1Controls = [W, A, S, D, SPACE]
        if (activeKeys.contains(tank1Controls[1])^activeKeys.contains(tank1Controls[3])) {
            tank1.incrementDirection(activeKeys.contains(tank1Controls[3]), time); // rotate clockwise
        }

        tank1.setMovement(activeKeys.contains(tank1Controls[0])^activeKeys.contains(tank1Controls[2]),
                activeKeys.contains(tank1Controls[0]));

        if (activeKeys.contains(tank1Controls[4])) {
            tank1.shoot(time);
        }
    }
    private void updateSlaveTankResponse() {
        if (tank2KeyInputs[1]^tank2KeyInputs[3]) { // A xor D
            tank2.incrementDirection(tank2KeyInputs[3], time); // rotate
        }

        tank2.setMovement(tank2KeyInputs[0]^tank2KeyInputs[2], tank2KeyInputs[0]); // W xor S

        if (tank2KeyInputs[4]) { // SPACE
            tank2.shoot(time);
        }
    }

///////////////////////////////////QUIT

    @Override protected void Quit() {
        // stop the game and processing loops
        processingLoop.cancel();
        gameLoop.stop();

        masterTanks.getStage().setTitle( "Master Tanks bruh" );
        System.out.println("Master done.");
        try {
            socket.close();
            listener.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            logFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // stop game music and masterTanks menu music
        if (enableMusic)
            musicPlayer.play("menu");

        masterTanks.getMultiplayer().setHostScene(); // go back to host page
    }
}