package csmasters.runtime;

import csmasters.setup.config;

import java.util.ArrayList;
import javafx.scene.image.Image;

public class spriteBullet extends sprite
{
    private double velocityX;
    private double velocityY;
    private int counter = 0;

	public spriteBullet(double x, double y, int direction) {
        String url = getClass().getResource( config.bulletURL ).toExternalForm();
		image = new Image( url );
        width = image.getWidth(); // for collision detection
        height = image.getHeight();
        positionX = x + 20 + 28* Math.sin(Math.PI * direction*2/config.rotateIncrements);
        positionY = y + 20 + 28* -1 * Math.cos(Math.PI * direction*2/config.rotateIncrements);
        velocityX = config.bulletSpeed * Math.sin(Math.PI * direction*2/config.rotateIncrements);
        velocityY = config.bulletSpeed * -1 * Math.cos(Math.PI * direction*2/config.rotateIncrements);

        xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
        numVertices = 4;
        // 0 = top right, 1 = bottom right, 2 = bottom left, 3 = top left
	}

    public void updatePosition(ArrayList<spriteWall> walls) {
        positionX += velocityX;
        for (int i = 0; i < 4; i++) { xVertices[i] += velocityX; }
        if (outOfBounds()) {
            positionX -= velocityX;
            for (int i = 0; i < 4; i++) { xVertices[i] -= velocityX; }
            velocityX = velocityX * -1;
        } else {
	        for (spriteWall wall : walls){
	        	if (wall.intersects(this)) {
	                positionX -= velocityX;
	                for (int i = 0; i < 4; i++) { xVertices[i] -= velocityX; }
	                velocityX = velocityX * -1;
	        	}
	        }
        }

        positionY += velocityY;
        for (int i = 0; i < 4; i++) { yVertices[i] += velocityY; }
        if (outOfBounds()) {
            positionY -= velocityY;
            for (int i = 0; i < 4; i++) { yVertices[i] -= velocityY; }
            velocityY = velocityY * -1;        	
        } else {
	        for (spriteWall wall : walls){
	        	if (wall.intersects(this)) {
	                positionY -= velocityY;
	                for (int i = 0; i < 4; i++) { yVertices[i] -= velocityY; }
	                velocityY = velocityY * -1;
	        	}
	    	}
        }
    }

    public boolean counter() {
    	counter++;
    	return counter > config.bulletLifespan;
    }
}
