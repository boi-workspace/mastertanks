package csmasters.runtime;

import csmasters.setup.config;

import javafx.scene.image.Image;
import java.util.ArrayList;
import java.util.Random;



public class spritePowerUp extends sprite
{

	private static Random R = new Random();
	private boolean operational;
	private int active;
	private int counter = 0;
	private boolean activated;
	private spriteTank currentTank;

	spritePowerUp() {
    	// initialize the powerup object, should not be active
        positionX = 100;
        positionY = 350;
		String url = getClass().getResource( config.powerUpURL ).toExternalForm();
        image = new Image(url);
        width = image.getWidth(); // for collision detection
        height = image.getHeight();
        operational = false;

        xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
		numVertices = 4;
        // 0 = top right, 1 = bottom right, 2 = bottom left, 3 = top left
    }
	boolean getOperational() { return operational; }
	void setOperational() { this.operational = false; }

	//Creates power ups in random locations and at random times, spawning variables available in csmasters.setup.config
	void powerUpGen(ArrayList<spriteWall> walls) {
		boolean validPos = true;
		int x = R.nextInt(976);
		int y = R.nextInt(720);

		active++;
		double powerUpLifespan = config.powerUpLifespan;
		if(active > powerUpLifespan) {
			active = 0;
			operational = false;
	        positionX = x;
	        positionY = y;
			xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
	        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
			for (spriteWall wall : walls){
				if (wall.intersects(this) && this.intersects(wall)) { // rotation is 0 which is parallel to the walls
		            validPos = false;
		        }
		    }

			if(validPos) {
		        operational = true;
			}
		}

		//Despawning the effects of the power up after 15 seconds
		if(activated) {
			counter++;
			if(counter >= powerUpLifespan) {
				currentTank.powerDown();
				counter = 0;
				activated = false;
			}
		}
	}

	boolean isShotBy(spriteBullet bullet) {
		return this.intersects(bullet);
	}
	boolean isRetrievedBy(spriteTank tank) {
		if(this.intersects(tank) & operational == true) {
			activated = true;
			operational = false;
			currentTank = tank;
			return true;
		} else {
			return false;
		}
	}
}
