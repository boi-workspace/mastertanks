package csmasters.runtime;

import csmasters.setup.config;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public abstract class sprite {
	Image image;
    double positionX;
    double positionY;
    double width;
    double height;
    // the hit-box vertex coordinates
    double[] xVertices;
    double[] yVertices;
    int numVertices = 4; // the number of vertices

    public void render(GraphicsContext gc) {
        gc.drawImage( image, positionX, positionY );
    }
    void drawVertices(GraphicsContext gc) {
    	// for debugging
		gc.setFill(Color.rgb(255,255,0));
		gc.fillOval(positionX, positionY, 2, 2);
    	gc.setFill(Color.WHITE);
    	gc.fillOval(xVertices[0], yVertices[0], 2, 2);
    	gc.setFill(Color.TEAL);
    	for(int i = 1; i < 4; i++){ gc.fillOval(xVertices[i], yVertices[i], 2, 2); }
    }

    boolean outOfBounds() {
        for (int i = 0; i < numVertices; i++) {
            if (xVertices[i] < 0 || xVertices[i]+width > config.xCanvasSize
                    || yVertices[i] < 0 || yVertices[i]+height > config.yCanvasSize) {
                return true;
            }
        }
    	return false;
    }

    double getX() {return positionX;}
    double getY() {return positionY;}
    double getXVertex(int i) {return xVertices[i];}
    double getYVertex(int i) {return yVertices[i];}
	double getSize() { return width; }
	// returns the center of the tank
	double getCenterX() { return positionX + width/2; }
	double getCenterY() { return positionY + width/2; }

    // checks if any of the sprite s' vertices are inside this sprite
    // this function is optimised for squares DO NOT USE for other polygon types
    boolean intersects(sprite s) {
    	// uses the method of separation axis aka SAT
    	// finds out if any of sprite s' vertices are within this sprite

    	double xVectorA; // vector A
    	double yVectorA;
    	double xVectorB; // vector B
    	double yVectorB;
    	double ALength;
    	double scalarProjection; // the scalar projections of vector B on vector A
    	int toSide; // for each of sprite s' vertices to one side of the vector being checked, this variable is incremented
    	// 2 vectors to be checked for each sprite
    	// assumes there is always 4 vertices and the first and that non-adjacent vectors are parallel (i.e. always makes a rectangle)
    	for (int i = 1; i < 4; i+=2){
    		// treat vertex 0 as the origin, vector A = vertex i - vertex 0
    		toSide = 0;
    		xVectorA = xVertices[i] - xVertices[0];
    		yVectorA = yVertices[i] - yVertices[0];
    		ALength = Math.sqrt(xVectorA*xVectorA + yVectorA*yVectorA); // |A|
        	for (int j = 0; j < 4; j++){
    			// vector B = vertex j relative to vertex 0 of this
        		xVectorB = s.getXVertex(j) - xVertices[0];
        		yVectorB = s.getYVertex(j) - yVertices[0];
        		scalarProjection = ( xVectorA*xVectorB + yVectorA*yVectorB ) // A.B
	    							/ ALength; // |A|
            	if (ALength < scalarProjection)
            		toSide++;
            	if (scalarProjection < 0) // the scalar projection is within the vector
            		toSide--;
    		}
    		if (toSide == 4 || toSide == -4)
    			return false; // all vertex projections are to one side of vector A
    	}
    	// if all projections overlap, the sprites are intesecting
    	return true;
    }
}









