package csmasters.runtime;

import csmasters.setup.config;

import javafx.scene.image.Image;

class spriteWall extends sprite
{
	spriteWall(int x, int y, int type) {
        String url = getClass().getResource( config.wallURL ).toExternalForm();
        url = url.substring(0,url.length()-5);
		image = new Image(url + type + ".png");

        width = image.getWidth();
        height = image.getHeight();
        positionX = x;
        positionY = y;

        xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
        numVertices = 4;
        // 0 = top right, 1 = bottom right, 2 = bottom left, 3 = top left
	}

	Boolean nodeProximity(int x, int y, double radius) {
	    // does this wall intersect a specified personal bubble?
        double vx, vy, mag; // vector between center and vertex and it's magnitude
        for (int i = 0; i < 4; i++) {
            vx = xVertices[i] - x;
            vy = yVertices[i] - y;
            mag = Math.sqrt(vx*vx + vy*vy);
            if (mag < radius)
                return true; // intersection!
        }
        return false;
    }
}
