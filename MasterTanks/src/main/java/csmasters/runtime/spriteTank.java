package csmasters.runtime; // the package that this is part of

import csmasters.setup.config;

import java.net.URL;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import java.util.ArrayList;


public class spriteTank extends sprite // TODO organise into catagories
{
	// POSITION AND ROTATION VARIABLES
    private double velocityX = 0;
    private double velocityY = 0;
    private double speed = config.tankSpeed; // magnitude of velocity
    private int speedDir = 0; // 1 = moving forwards, 0 = staying still, -1 = moving backwards
    private int direction; // can be from 0 to 15, 0 = up, 4 = facing right, 12 = facing left
    private int invun = config.tankInvun; // counter for respawn invulnerability

	// POWER-UP AND SHOOTING VARIABLES
	private int power;
	private boolean powerActive = false;
	boolean bubbleShield = false;
	private int timeLastShot = 0;
	private double fireRate = 1;
	private int timeLastRotated = 0;

	// LOADED ASSETS
	private Image[] animation = new Image[4];
	private Image[] animationShooting = new Image[4];
	private int animationTimer, animationSize, animationFrames, animationFramesDefault;
	private Boolean isShooting = false;
	private int tankImageNum; // which tank is called
	private MediaPlayer respawnSound;
	private Media media;
	private ArrayList<spriteBullet> bullets; // because of the way arraylists work,
	// this is the same list instance as the one in run ¯\_(ツ)_/¯

    spriteTank(int x, int y, int tank, int direction) {
    	// initialize the tank object
        positionX = x;
        positionY = y;

		animationTimer = 0;
		try {
			if (tank == 0) { // PANDA
				String url = getClass().getResource( config.pandaURL ).toExternalForm();
				url = url.substring(0,url.length()-5);
				String shooturl = getClass().getResource( config.pandaShootURL ).toExternalForm();
				shooturl = shooturl.substring(0,shooturl.length()-5);

				animationSize = 4;
				for (int i = 0; i < animationSize; i++) {
					animation[i] = new Image(url + i + ".png");
					animationShooting[i] = new Image(shooturl + i + ".png");
				}
				animationFrames = 2;
			} else {
				String url = getClass().getResource( config.tankURL ).toExternalForm();
				url = url.substring(0,url.length()-5);

				animationSize = 2;
				animation[0] = new Image(url + tank + ".png");
				animation[1] = new Image(url + tank + "alt.png");
				animationShooting[0] = new Image(url + tank + ".png");
				animationShooting[1] = new Image(url + tank + "alt.png");
				if (tank == 5)
					animationFrames = 10;
				else
					animationFrames = 3;
			}
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		image = animation[0];
		animationFramesDefault = animationFrames; // for power-up restoring
		tankImageNum = tank;

        this.direction = direction;

        width = image.getWidth(); // for collision detection
        height = image.getHeight();

        xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
        yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
        numVertices = 4;
        // 0 = top right, 1 = bottom right, 2 = bottom left, 3 = top left

		URL url = getClass().getResource(config.shootSoundURL);
        media = new Media(url.toString());
        respawnSound = new MediaPlayer(media);
    }
    public void render(GraphicsContext gc) {
    	gc.save(); // saves the current state on stack, including the current transform
    	double angle = direction*360/config.rotateIncrements;
        Rotate r = new Rotate(angle, positionX + image.getWidth()/2, positionY + image.getHeight()/2);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());

		draw(gc);

        gc.restore(); // un-does the rotation transform that was applied to the canvas

        // if the tank has a power up, it will have a coloured rectangle around it
        // colour of rectangle:
        switch(power) { // is break needed?
        	case 0: gc.setStroke(Color.rgb(18,200,151)); break; // bubble shield
        	case 1: gc.setStroke(Color.rgb(255,66,167)); break; // + fire rate
        	case 2: gc.setStroke(Color.rgb(128,88,255)); break; // - fire rate
        	case 3: gc.setStroke(Color.rgb(255,83,46)); break; // + movement
        	case 4: gc.setStroke(Color.rgb(172,217,0)); break; // - movement
        }
    	if (powerActive)
    		gc.strokePolygon(xVertices, yVertices, 4);

		if(config.debuggingMode)
			drawVertices(gc);
    }
    private void draw(GraphicsContext gc) {
		// if the tank has just respawned, it will flash to show it is invunerable
		if (invun < config.tankInvun) {
			invun++;
			if ((invun/3)%3 > 1)
				return;
		}

		if (tankImageNum == 5 || velocityX + velocityY != 0) {
			updateAnimationTimer();
		}

		// which image to render
		int imageIndex = (animationTimer / animationFrames) % animationSize;
		if (!isShooting) {
			gc.drawImage(animation[imageIndex], positionX, positionY);
		} else {
			gc.drawImage(animationShooting[imageIndex], positionX, positionY);
			isShooting = false;
		}
	}
	private void updateAnimationTimer() {
		animationTimer++;
	}



///////////////////// UPDATE POSITION AND DIRECTION

	// velocity
    void setMovement(boolean moving, boolean forwards) {
    	// updates the tank's movement instructions
    	if (!moving) {
    		speedDir = 0;
    	} else if (forwards) {
    		speedDir = 1;
    	} else {
    		speedDir = -1;
    	}
    }
    void updateVelocity() {
    	// possible directions that the tank can point are restricted to multiples of pi/8
        velocityX = speed * speedDir * Math.sin(Math.PI * direction*2/config.rotateIncrements);
        velocityY = speed * speedDir * -1 * Math.cos(Math.PI * direction*2/config.rotateIncrements);
    }
    void increaseSpeed() { speed *= 1.1; }

    // position updates
    void updatePosition(ArrayList<spriteWall> walls) {

        positionX += velocityX;
        positionY += velocityY;
        for (int i = 0; i < 4; i++) {
        	xVertices[i] += velocityX;
        	yVertices[i] += velocityY;
        }

		// if the tank has just moved out of bounds or into a wall, we must push it back
		moveIntoBounds();

		// return true if there was a wall collision
        moveOutOfWalls(walls);
    }
    void setPosition(int positionX, int positionY, int direction) {
    	if (this.positionX != positionX || this.positionY != positionY || this.direction != direction){
			updateAnimationTimer(); // enables animation
		}
        this.positionX = positionX;
        this.positionY = positionY;
        this.direction = direction;
		updateVertices();
    }
	private void updateVertices() {
		for (int i = 0; i < 4; i++) {
			// position + (24,24) is the center of the image
			xVertices[i] = positionX+24 + Math.sqrt(2)*24*Math.cos(
					Math.PI*2/config.rotateIncrements*(direction+i*config.rotateIncrements/4) -
							Math.PI*4/config.rotateIncrements);
			yVertices[i] = positionY+24 + Math.sqrt(2)*24*Math.sin(
					Math.PI*2/config.rotateIncrements*(direction+i*config.rotateIncrements/4) -
							Math.PI*4/config.rotateIncrements);
		}
	}

	// rotation
	void incrementDirection (boolean cw, int time) {
		updateAnimationTimer();
    	// rotates the tank
		if(time - timeLastRotated > config.rotateDelay) {
			rotate(cw);
			// if out of bounds now, the tank will be moved back in
			moveIntoBounds();
			timeLastRotated = time;
		}
	}
	private void rotate(boolean cw) {
		// true for clockwise increment
		if (cw) {
			if (direction == config.rotateIncrements - 1){
				direction = 0;
			} else {
				direction++;
			}
		} else {
			if (direction == 0){
				direction = config.rotateIncrements - 1;
			} else {
				direction--;
			}
		}
		updateVertices();
	}
	int getDirection() { return direction; }
	void setDirection(int d) {
		// will rotate towards to direction d
		direction = d;
		// update hit box
		updateVertices();
	}



///////////////////// COLLISION DETECTION

	// handles the bounds of the screen
	private void moveIntoBounds() {
		// this function will update position data such that the tank will be inside the bounds.
		double moveX = pushVectorIntoBounds(true);
		double moveY = pushVectorIntoBounds(false);
		positionX -= moveX;
		positionY -= moveY;
		for (int i = 0; i < 4; i++) {
			xVertices[i] -= moveX;
			yVertices[i] -= moveY;
		}
	}
	private double pushVectorIntoBounds(boolean x) { // true for x direction and false for y
		double push = 0;
		for (int i = 0; i < 4; i++){
			if (x) {
	        	if (xVertices[i] < 0 && xVertices[i] < push)
	    			push = xVertices[i];
	        	if (xVertices[i] > config.xCanvasSize && xVertices[i] > push)
	    			push = xVertices[i] - config.xCanvasSize;
			} else {
	        	if (yVertices[i] < 0 && yVertices[i] < push)
	    			push = yVertices[i];
	        	if (yVertices[i] > config.yCanvasSize && yVertices[i] > push)
	    			push = yVertices[i] - config.yCanvasSize;
			}
		}
    	return push;
    }

    // handles collisions with walls
	private void moveOutOfWalls(ArrayList<spriteWall> walls) {
    	double moveX, moveY;
		double[] mtv;

		for (spriteWall wall : walls) { // cycle through all walls (in order)
			mtv = mtv(wall);
			moveX = mtv[0];
			moveY = mtv[1];

			positionX -= moveX;
			positionY -= moveY;
			for (int i = 0; i < 4; i++) {
				xVertices[i] -= moveX;
				yVertices[i] -= moveY;
			}
		}
	}
	private double[] mtv(sprite s) {
    	// returns x and y
		double[] mtv = {0,0};

		double xVectorA, yVectorA, xVectorB, yVectorB, ALength;
		double scalarProjection, bMin, bMax, overlap;
		double smallestOverlap = 420;

		// for the tank's two axes
		for (int i = 1; i < 4; i+=2){
			// treat vertex 0 as the origin, vector A = vertex i - vertex 0
			xVectorA = xVertices[i] - xVertices[0]; // vector A = 1 of the 2 edges of this
			yVectorA = yVertices[i] - yVertices[0];
			ALength = Math.sqrt(xVectorA*xVectorA + yVectorA*yVectorA); // |A|

			// find bMin and bMax out of the 4 projections
			bMin = 420;
			bMax = -420;
			for (int j = 0; j < 4; j++){
				// vector B = vertex j relative to vertex 0 of this
				xVectorB = s.getXVertex(j) - xVertices[0];
				yVectorB = s.getYVertex(j) - yVertices[0];
				scalarProjection = ( xVectorA*xVectorB + yVectorA*yVectorB ) // A.B
						/ ALength; // |A|
				if( scalarProjection < bMin ){ bMin = scalarProjection; }
				if( bMax < scalarProjection ){ bMax = scalarProjection; }
			}
			overlap = overlap(ALength, bMin, bMax);

			// if one axis has no overlap, the sprites are not intersecting ∴ do not move them.
			if (overlap == 0) {
				mtv[0] = 0; mtv[1] = 0;
				return mtv;
			}
			if ( Math.abs(overlap) < Math.abs(smallestOverlap) ) {
				smallestOverlap = overlap;
				mtv[0] = smallestOverlap * xVectorA/ALength;
				mtv[1] = smallestOverlap * yVectorA/ALength;
			}
		}

		// for the sprite's two axes
		for (int i = 1; i < 4; i+=2){
			// treat vertex 0 as the origin, vector A = vertex i - vertex 0
			xVectorA = s.getXVertex(i) - s.getXVertex(0); // vector A = 1 of the 2 edges of this
			yVectorA = s.getYVertex(i) - s.getYVertex(0);
			ALength = Math.sqrt(xVectorA*xVectorA + yVectorA*yVectorA); // |A|

			// find bMin and bMax out of the 4 projections
			bMin = 420;
			bMax = -420;
			for (int j = 0; j < 4; j++){
				// vector B = vertex j relative to vertex 0 of this
				xVectorB = xVertices[j] - s.getXVertex(0);
				yVectorB = yVertices[j] - s.getYVertex(0);
				scalarProjection = ( xVectorA*xVectorB + yVectorA*yVectorB ) // A.B
						/ ALength; // |A|
				if( scalarProjection < bMin ){ bMin = scalarProjection; }
				if( bMax < scalarProjection ){ bMax = scalarProjection; }
			}
			overlap = -overlap(ALength, bMin, bMax);

			// if one axis has no overlap, the sprites are not intersecting ∴ do not move them.
			if (overlap == 0) {
				mtv[0] = 0; mtv[1] = 0;
				return mtv;
			}
			if ( Math.abs(overlap) < Math.abs(smallestOverlap) ) {
				smallestOverlap = overlap;
				mtv[0] = smallestOverlap * xVectorA/ALength;
				mtv[1] = smallestOverlap * yVectorA/ALength;
			}
		}

		return mtv;
	}
	private double overlap(double a, double bMin, double bMax) {
    	// returns the overlap between two lines: A = a - 0, and B = bMax - bMin
		// assumes A is the tank and B is the obsticle, such that A is pushed out of B by the overlap value
		// if B is the sprite you want to move, inverse the output.
    	if ( bMax <= 0 && a <= bMin ) { // A within B
    		return a;
		} else if ( 0 <= bMin && bMax <= a ) { // B within A
    		return bMax - bMin;
		} else if ( bMax < 0 || a < bMin ){ // B outside A ∴ no overlap
			return 0;
		} else if ( bMin < 0 ){ // B below A, positive output, A moves in positive direction
			return -bMax;
		} else { // B above A, negative output, A moves in negative direction
    		return a - bMin;
		}
	}

	// handles collisions with other tanks
	boolean tanksCollide(ArrayList<spriteWall> walls, spriteTank tank, GraphicsContext gc) {
		// respawn if the tanks touch each other
		if (intersects(tank) && tank.intersects(this)) {
			this.respawn(walls, tank, gc);
			tank.respawn(walls, this, gc);
			return true;
		}
		return false;
	}
	boolean tanksCollide(ArrayList<spriteWall> walls, spriteTank[] tanks, GraphicsContext gc) {
		// respawn if the tanks touch each other
		for(spriteTank tank: tanks){
			if (intersects(tank) && tank.intersects(this)) {
				this.respawn(walls, tank, gc);
				tank.respawn(walls, this, gc);
				return true;
			}
		}
		return false;
	}



///////////////////// BULLET HANDELLING

	void loadBullets(ArrayList<spriteBullet> bullets) {
		// shooting is handled by the tank sprite, but the bullets need to be loaded into the csmasters.runtime.run class
		this.bullets = bullets;
	}

	// shooting and bullet collision detection
	void shoot(int time) {
		isShooting = true; // runs a different animation while shooting
		// shoot delay controls the rate of fire
		if(time - timeLastShot > config.shootDelay / fireRate) {
			bullets.add(new spriteBullet(positionX, positionY, direction));
			timeLastShot = time;
		}
	}
    boolean isShotBy(spriteBullet bullet) {
    	if (invun < config.tankInvun) {
    		return false;
    	} else {
    		return this.intersects(bullet);
    	}
    }

    // respawning
	void respawn(ArrayList<spriteWall> walls, spriteTank tank, GraphicsContext gc) {
        respawnSound.play();
        respawnSound = new MediaPlayer(media);
    	gc.setFill(Color.RED);
        gc.fillPolygon(xVertices, yVertices, 4);

        boolean validPos = false;
        while (!validPos) {
        	validPos = true;
            positionX = Math.random()*(config.xCanvasSize-48);
            positionY = Math.random()*(config.yCanvasSize-48);
			updateVertices();
            if ((tank.intersects(this) && this.intersects(tank)))
            	validPos = false;
            else {
	        	for (spriteWall wall : walls) {
	            	if (wall.intersects(this) && this.intersects(wall)) { // rotation is 0 which is parallel to the walls
	            		validPos = false;
	            	}
	            }
            }
        }
        invun = 0;
    }
    public void respawn(ArrayList<spriteWall> walls, spriteTank[] tanks, GraphicsContext gc) {
        respawnSound.play();
        respawnSound = new MediaPlayer(media);
    	gc.setFill(Color.RED);
        gc.fillPolygon(xVertices, yVertices, 4);

        direction = 0;
        boolean validPos = false;
        while (!validPos) {
        	validPos = true;
            positionX = Math.random()*(config.xCanvasSize-48);
            positionY = Math.random()*(config.yCanvasSize-48);
            xVertices = new double[] {positionX + width, positionX + width, positionX, positionX};
            yVertices = new double[] {positionY, positionY + height, positionY + height, positionY};
            if ((tanks[0].intersects(this) && this.intersects(tanks[0]))
            		 		  || (tanks[1].intersects(this) && this.intersects(tanks[1]))
            				  || (tanks[2].intersects(this) && this.intersects(tanks[2])))
            	validPos = false;
            else {
	        	for (spriteWall wall : walls){
	            	if (wall.intersects(this) && this.intersects(wall)) { // rotation is 0 which is parallel to the walls
	            		validPos = false;
	            	}
	            }
            }
        }
        invun = 0;
    } // TODO this should be used in 1v3



///////////////////// POWER-UP HANDELLING

    //Gives the tank a random power up for 15 seconds
	void powerUp() {
    	//Removes current power
		if (powerActive)
			powerDown();

		powerActive = true;
    	power = (int) (Math.random() * 5); //Randomly selects a power

    	switch(power) {
	    	case 0: bubbleShield = true; break; // bubble shield
	    	case 1: fireRate = 3; break; // + fire rate
	    	case 2: fireRate = 0.4; break; // - fire rate
	    	case 3: speed *= 1.5;
				animationFrames--;
				break; // + movement
	    	case 4: speed *= 0.5;
				animationFrames+=3;
				break; // - movement
	    	default: break;
	    }
    }
    //Removes the effects of the power up
    void powerDown() {
    	switch(power) {
	    	case 0: bubbleShield = false; break; // bubble shield
	    	case 1: fireRate = 1; break; // + fire rate
	    	case 2: fireRate = 1; break; // - fire rate
	    	case 3: speed = config.tankSpeed;
	    		animationFrames = animationFramesDefault;
				break; // + movement
	    	case 4: speed = config.tankSpeed;
				animationFrames = animationFramesDefault;
				break; // - movement
	    	default: break;
	    }
    	powerActive = false;
    }
}


