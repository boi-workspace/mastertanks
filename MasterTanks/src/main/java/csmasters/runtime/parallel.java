/*
This class is used to run complex and/or time-consuming processing operations
in another thread to the timeline one when in game. Timeline is set to a rigid
30fps structure and if processing exceeds 1/30s the game will not be happy.
If the code in run.parallelProcessing() takes longer than 1/30s then this loop
will not freak out and simply run the next itteration right after it's complete,
such is the nature of Sceduled Service!
 */

package csmasters.runtime;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

public abstract class parallel extends ScheduledService<Void> {
    @Override
    protected Task<Void> createTask() {
        return new Task<>() {
            @Override
            protected Void call() {
                parallelProcessing();
                return null;
            }
        };
    }

    abstract protected void parallelProcessing();
}
