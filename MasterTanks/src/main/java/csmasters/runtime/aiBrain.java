/*
A collection of methods to handle tank logic
 */

package csmasters.runtime;

import csmasters.setup.config;

import java.util.ArrayList;

public abstract class aiBrain implements config {
    // variables for shootLogicTrackDown()
    private static double shootPeriod = 1; // the number of s in between tank shots (1)
    private static double delayTime = 0.3; // the delay before first shot (0.3)
    private static int lastShootTime = 0;
    // general variables
    private static int tankD = 0;

    // difficulty modifiers
    public static void setDifficulty(int difficulty) {
        // 0 = easy, 1 = normal, 2 = hard
        switch (difficulty) {
            case 0: // easy
                shootPeriod = 1.5;
                delayTime = 0.7;
                break;
            case 1: // normal
                shootPeriod = 1;
                delayTime = 0.3;
                break;
            case 2: // hard
                shootPeriod = secondsPerFrame;
                delayTime = 0;
                break;
            default:
                shootPeriod = 1;
                delayTime = 0.3;
                break;
        }
    }

    static void tankAI(spriteTank tankSelf, spriteTank tankTarget, int[] targetPos, ArrayList<spriteWall> walls, int time) {
        // calculate the closest d of the target tank for the other functions to use
        double targetAngle = angle(tankSelf.getCenterX(),tankSelf.getCenterY(),
                tankTarget.getCenterX(),tankTarget.getCenterY());
        tankD = angleToD(targetAngle);

        // if we have a clear path to shoot the target
        if (lineOfSight(tankSelf,tankTarget,walls)) {
            // shoot logic
            shootLogicTrackDown(tankSelf,tankTarget,time);
        } else {
            // move to the targetPos, which was calculated to get tankSelf to tankTarget
            aiBrain.goTo(tankSelf, targetPos[0], targetPos[1], time);
        }
    }

    private static Boolean lineOfSight(sprite self, sprite target, ArrayList<spriteWall> walls) {
        // vector between self and target
        double vectorX = self.getX() - target.getX();
        double vectorY = self.getY() - target.getY();
        double magnitude = Math.sqrt(vectorX*vectorX + vectorY*vectorY);
        // a vector perpendicular to the one above
        double perpX = vectorY/magnitude*self.getSize()*0.4;
        double perpY = -1*vectorX/magnitude*self.getSize()*0.4;

        // make a polygon encompasing both self and the target
        aiLineOfSightPolygon losp = new aiLineOfSightPolygon(
                new double[] {self.getX()+24+perpX, target.getX()+24+perpX,
                        target.getX()+24-perpX, self.getX()+24-perpX},
                new double[] {self.getY()+24+perpY, target.getY()+24+perpY,
                        target.getY()+24-perpY, self.getY()+24-perpY} );

        // check if there are an walls inbetween self and the target
        for (spriteWall wall: walls) {
            if (losp.intersects(wall))
                return false; // path is obstructed
        }
        return true; // there is a line of sight between self and the target
    }

    private static void shootLogicTrackDown(spriteTank tankSelf, spriteTank tankTarget, int time) {
        // moves to a position where a shot is ensured before shooting
        if (canHit(tankSelf,tankTarget)) {
            tankSelf.setMovement(false, true); // stop
            shootTarget(tankSelf, time);
        } else {
            // move until in range babeeeey (fowards or backwards)
            //int directionDifference = Math.abs(tankSelf.getDirection()-tankD);
            //Boolean fowards = directionDifference < rotateIncrements/4;
            if (!dWithinRange(tankSelf.getDirection(),tankD,2)) {
                // rotate to approximately face the target
                Boolean rotateClockwise = rotateClockwise(tankSelf.getDirection(),tankD);
                tankSelf.incrementDirection(rotateClockwise,time); // rotate
            } else {
                // move foward until in shooting range
                tankSelf.setMovement(true, true);
            }
        }
    }
    private static Boolean canHit(spriteTank tankSelf, spriteTank tankTarget) {
        // can we hit the target tank or do we need to move in closer?

        // the angle between where the tank is currently pointing and the target
        double targetAngle = angle(tankSelf.getCenterX(),tankSelf.getCenterY(),
                tankTarget.getCenterX(),tankTarget.getCenterY());
        // round angle to the closest multiple of 2pi/rotateIncrements
        double roundedAngle = Math.round(targetAngle/(Math.PI*2)*rotateIncrements)
                /(double)rotateIncrements*(Math.PI*2);

        // convert the angle into a vector
        double vectorX = Math.sin(roundedAngle);
        double vectorY = -Math.cos(roundedAngle);
        // convert to a perpindicular vector
        double temp = vectorX;
        vectorX = -vectorY;
        vectorY = temp;

        // find the scalar projection of the center of the target tank on the perpedicular d vector
        double targetX = tankTarget.getCenterX() - tankSelf.getCenterX();
        double targetY = tankTarget.getCenterY() - tankSelf.getCenterY();
        // scalar projection of A on B = dot(A,B)/mag(B); in this case mag(B) = 1
        double scalarProjection = (vectorX*targetX + vectorY*targetY);

        // check if the body of the target tank is inside our d vector
        return scalarProjection-tankTarget.getSize()/1.9 < 0 && 0 < scalarProjection+tankTarget.getSize()/1.9;
    }

    static void goTo(spriteTank tank, double xTarget, double yTarget, int time) {
        // the angle between where the tank is currently pointing and the target
        double targetAngle = angle(tank.getCenterX(),tank.getCenterY(),xTarget,yTarget);
        // convert the angle to the closest d for the tank
        int targetD = angleToD(targetAngle);
        // rotate either clockwise or anticlockwise?
        Boolean rotateClockwise = (targetD-tank.getDirection()+rotateIncrements)%rotateIncrements
                                < (tank.getDirection()-targetD+rotateIncrements)%rotateIncrements;

        if (tank.getDirection() == targetD) {
            // we are already facing the target
            tank.setMovement(true, true); // drive forwards
        } else if (targetD-2 < tank.getDirection() && tank.getDirection() < targetD+2) {
            // if we are approximately facing the direction of the target
            tank.setMovement(true, true); // drive forwards
            tank.incrementDirection(rotateClockwise,time); // rotate
        } else {
            // else pause and rotate until target angle is within +-2Δθ
            tank.setMovement(false, true); // stop
            tank.incrementDirection(rotateClockwise,time); // rotate
        }
    }

//////////////////////////////////// HELPER METHODS

    private static double angle(double xSelf, double ySelf, double xTarget, double yTarget) {
        // vector pointing up (which corresponds to d = 0)
        int v1x = 0;
        int v1y = -1;
        // the vector between tank and target
        double v2x = xTarget - xSelf;
        double v2y = yTarget - ySelf;
        double dot = v1x*v2x + v1y*v2y; // dot product
        double det = v1x*v2y - v1y*v2x; // determinant
        return Math.atan2(det, dot);
        // we can't use acos because it's range is only from 0 to pi
    }
    private static int angleToD(double angle) { // angle in radians
        double a = Math.round(angle/(Math.PI*2) * rotateIncrements) + rotateIncrements;
        return (int)a % rotateIncrements;
    }
    private static void shootTarget(spriteTank tankSelf, int time) {
        // shoots at the target
        tankSelf.setMovement(false, true); // stop

        // rotate either clockwise or anticlockwise?
        Boolean rotateClockwise = rotateClockwise(tankSelf.getDirection(),tankD);

        int periodFrames = (int) (shootPeriod / secondsPerFrame);
        int delayFrames = (int) (delayTime / secondsPerFrame);
        if (tankSelf.getDirection() == tankD) {
            // shoot them!
            if (time-lastShootTime > periodFrames*2 || time-lastShootTime < 0) {
                // slight delay after it finds you
                lastShootTime = time-(periodFrames-delayFrames);
            } else if (time-lastShootTime > periodFrames) {
                tankSelf.shoot(time);
                lastShootTime = time;
            }
        } else {
            // else rotate to targetD
            tankSelf.incrementDirection(rotateClockwise,time); // rotate
        }
    }
    private static Boolean rotateClockwise(int selfD, int targetD) {
        return (targetD-selfD+rotateIncrements)%rotateIncrements
                < (selfD-targetD+rotateIncrements)%rotateIncrements;
    }
    private static Boolean dWithinRange(int selfD, int targetD, int range) {
        int difference = Math.min(Math.abs((targetD-selfD+rotateIncrements)%rotateIncrements)
                ,Math.abs((selfD-targetD+rotateIncrements)%rotateIncrements));
        return difference < range;
    }
}
