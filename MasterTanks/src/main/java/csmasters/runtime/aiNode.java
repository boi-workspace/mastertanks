/*
https://www.youtube.com/watch?v=-L-WgKMFuhE
http://www.jgallant.com/nodal-pathfinding-in-unity-2d-with-a-in-non-grid-based-games/
щ(ಠ益ಠщ)
 */

package csmasters.runtime;

import csmasters.setup.config;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.ArrayList;

class aiNode {
    private int[] pos = new int[2]; // [x,y] co-ordinates, note that this corresponds to the center of the sprite
    private ArrayList<aiNode> adjacents = new ArrayList<>(); // pointers to nodes in the 8 directions (can be null)
    private ArrayList<Integer> directions = new ArrayList<>(); // the directions of each adjacent
    // range = (0 - 7), 0 is up, move clockwise in 45 degree incriments.
    private aiAStar AI;

    private Boolean isEvaluated = false, isDiscovered = false;
    private Boolean isStart = false, isTarget = false;
    private int G; // distance of path taken from masterTanks
    private int H; // shortest distance (via nodes) to target
    private int F; // G + H
    private int parentDirection = -1; // direction of where it's parent node is (leads back to masterTanks), -1 if node is masterTanks

//////////////////////////////////// INITIALIZATION

    aiNode(int x, int y, aiAStar AI) {
        pos[0] = x; pos[1] = y; this.AI = AI;
    }
    void addAdjacent(int direction, aiNode neighbour) {
        // pointers on both ends
        // add neighbour to this
        add(direction, neighbour);
        // add this to neighbour
        neighbour.add((direction+4)%8, this); // opposite direction
    }
    private void add(int direction, aiNode neighbour) { // TODO: can this and remove be made private?
        if (!adjacents.contains(neighbour)) {
            adjacents.add(neighbour);
            directions.add(direction);
        }
    }
    void removeAdjacents() {
        int i = 0;
        while (i < directions.size()) {
            // remove this from neighbour
            adjacents.get(i).remove((directions.get(i) + 4) % 8); // opposite direction
            // remove neighbour from this
            adjacents.remove(i);
            directions.remove(i);
        }
    }
    private void remove(int direction) {
        int index = directions.indexOf(direction);
        if (index != -1) {
            adjacents.remove(index);
            directions.remove(index);
        }
    }

    int getF() {return F;}
    int getH() {return H;}
    int[] getPos() {return pos;}
    aiNode getParent() {
        if (parentDirection == -1)
            return null;
        return adjacents.get(directions.indexOf(parentDirection));
    }

//////////////////////////////////// DISCOVERY

    void discoverStart() {
        isStart = true;
        this.parentDirection = -1;
        G = 0; H = 0; F = 0;
        AI.getNodesOpen().add(this);
        isDiscovered = true;
    }
    void setTarget() {
        isTarget = true;
    }
    private void discover(int parentDirection) {
        int oldParentDirection = this.parentDirection;
        this.parentDirection = parentDirection;
        int newG = calculateG(0,0);
        if (oldParentDirection == -1 || newG < G) {
            // we want the parent direction with smallest G (H will be the same regardless)
            G = newG;
        } else {
            this.parentDirection = oldParentDirection;
        }
        calculateH();
        F = G + H;
        //System.out.println("id:"+id+",G:"+G+",H:"+H+",F:"+F+",pd:"+parentDirection);

        AI.getNodesOpen().add(this);
        isDiscovered = true;
    }
    private int calculateG(int G, int c) {
        // recursive function: failsafe return
        int count = c + 1;
        if (count > 10000) {
            System.out.println("A* PATHPLANNING ERROR: INFINITE LOOP [aiNode->calculateG]");
            return -1;
        }

        // find distance of the path taken from the masterTanks to the evaluated node
        if (isStart)
            return G;
        int index = directions.indexOf(parentDirection);
        if (index != -1) {
            // either add 10 or 14 to G (14~=10sqrt(2))
            if (parentDirection%2 == 0) // straight translation
                return adjacents.get(index).calculateG(G + 10, count);
            else // diagonal
                return adjacents.get(index).calculateG(G + 14, count);
        } else {
            System.out.println("A* PATHPLANNING ERROR: INVALID PARENT DIRECTION [aiNode->calculateG]");
            return 0;
        }
    }
    private void calculateH() { // shortest distance (via nodes) to target
        int x = this.pos[0], y = this.pos[1]; // p
        int vx = AI.getTarget().pos[0] - x; // vector from this to target
        int vy = AI.getTarget().pos[1] - y;
        double distanceTo = Math.sqrt(vx*vx + vy*vy); // magnitude of vector
        double theta = Math.atan2(vy,vx);
        H = 0;

        while (distanceTo > 1) { // TODO: back up break case?
            int d = ((int)Math.round(theta*4/Math.PI)+6)%8; // convert theta -> closest 45 degree incriment -> d

            // either add 10 or 14 to H (14~=10sqrt(2))
            if (d%2 == 0) // straight translation
                H += 10;
            else // diagonal
                H += 14;

            // update data for next itteration
            int addx = config.nodeSize, addy = config.nodeSize;
            switch (d) {
                case 0:
                    addx*=0;  addy*=-1; break;
                case 1:
                    addx*=1;  addy*=-1; break;
                case 2:
                    addx*=1;  addy*=0;  break;
                case 3:
                    addx*=1;  addy*=1;  break;
                case 4:
                    addx*=0;  addy*=1;  break;
                case 5:
                    addx*=-1; addy*=1;  break;
                case 6:
                    addx*=-1; addy*=0;  break;
                case 7:
                    addx*=-1; addy*=-1; break;
                default:
                    addx*=0;  addy*=0;  break;
            }
            //gc.strokeLine(x,y,x-addx,y-addy); for debugging
            x -= addx; y -= addy;
            vx = AI.getTarget().pos[0] - x; // vector from this to target
            vy = AI.getTarget().pos[1] - y;
            distanceTo = Math.sqrt(vx*vx + vy*vy); // magnitude of vector
            theta = Math.atan2(vy,vx);
        }
    }

//////////////////////////////////// EVALUATION

    Boolean evaluate() {
        //System.out.println("F:"+F+",G:"+G+",H:"+H+",pd:"+parentDirection);
        AI.getNodesOpen().remove(this);
        AI.getNodesClosed().add(this);
        isEvaluated = true;

        // discover adjacents (that haven't been evaluated yet)
        for (aiNode neighbour: adjacents) {
            int childDirection = directions.get(adjacents.indexOf(neighbour));
            if (!neighbour.isEvaluated && !neighbour.isDiscovered)
                neighbour.discover((childDirection+4)%8);
            if (neighbour.isTarget) {
                return true; // target found
            }
        }
        return false; // target not found
    }
    aiNode nodeGoTo() {
        if (isStart)
            return this;
        // returns the next node for the tank to go to. only called once target is reached
        aiNode nextNode = getParent();
        if (nextNode.isStart) {
            // returns the next node after the masterTanks sprite (yourself)
            return this;
        } else {
            return nextNode.nodeGoTo();
        }
    }
    aiNode nodeGoToSmoothed() {
        if (isStart)
            return this;

        // this function is designed to be called by the target node
        int direction = parentDirection;
        aiNode gotoNode = this;
        aiNode node = getParent();

        // follow node path until the masterTanks is reached
        while (!node.isStart) {
            // if a corner is reached in the path
            if (direction != node.parentDirection) {
                gotoNode = node;
                direction = node.parentDirection;
            }
            // go to next node
            node = node.getParent();
        }

        // returns the node at the end of a path of nodes in the same direction
        return gotoNode;
    }

//////////////////////////////////// CLOSING AND DEBUGGING

    void resetNode() {
        parentDirection = -1;
        isDiscovered = false; isEvaluated = false; isStart = false; isTarget = false;
    }

    void draw(GraphicsContext gc) {
        // for debugging
        if (isEvaluated) {
            drawParentDirection(gc);
            gc.setFill(Color.rgb(255,0,0));
            gc.fillRect(pos[0], pos[1], 3, 3);
            gc.setStroke(Color.rgb(255,0,0));
            drawValues(gc);
        } else if (isDiscovered) {
            //drawDirections(gc);
            drawParentDirection(gc);
            gc.setFill(Color.rgb(0,255,0));
            gc.fillRect(pos[0], pos[1], 3, 3);
            gc.setStroke(Color.rgb(0,255,0));
            drawValues(gc);
        } else {
            gc.setFill(Color.rgb(200,200,200));
            gc.fillOval(pos[0], pos[1], 2, 2);
        }
    }
    private void drawDirections(GraphicsContext gc) {
        for (int d: directions) {
            int x = 10, y = 10;
            switch (d) {
                case 0:
                    x*=0; y*=-1; break;
                case 1:
                    x*=1; y*=-1; break;
                case 2:
                    x*=1; y*=0; break;
                case 3:
                    x*=1; y*=1; break;
                case 4:
                    x*=0; y*=1; break;
                case 5:
                    x*=-1; y*=1; break;
                case 6:
                    x*=-1; y*=0; break;
                case 7:
                    x*=-1; y*=-1; break;
                default:
                    x*=0; y*=0; break;
            }
            gc.setStroke(Color.rgb(0,255,255));
            gc.strokeLine(pos[0],pos[1],pos[0]+x,pos[1]+y);
        }
    }
    private void drawParentDirection(GraphicsContext gc) {
        int x = 10, y = 10;
        switch (parentDirection) {
            case 0:
                x*=0; y*=-1; break;
            case 1:
                x*=1; y*=-1; break;
            case 2:
                x*=1; y*=0; break;
            case 3:
                x*=1; y*=1; break;
            case 4:
                x*=0; y*=1; break;
            case 5:
                x*=-1; y*=1; break;
            case 6:
                x*=-1; y*=0; break;
            case 7:
                x*=-1; y*=-1; break;
            default:
                x*=0; y*=0; break;
        }
        gc.setStroke(Color.rgb(0,255,255));
        gc.strokeLine(pos[0],pos[1],pos[0]+x,pos[1]+y);
    }
    private void drawValues(GraphicsContext gc) {
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(new Font(10));
        gc.strokeText(Integer.toString(G),pos[0]-10,pos[1]-10);
        gc.strokeText(Integer.toString(H),pos[0]+10,pos[1]-10);
        gc.setFont(new Font(13));
        gc.strokeText(Integer.toString(F),pos[0],pos[1]+10);
    }
}
