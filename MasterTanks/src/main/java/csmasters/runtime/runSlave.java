package csmasters.runtime;

import csmasters.setup.masterTanks;
import csmasters.setup.musicPlayer;
import csmasters.multiplayer.mpControllerClient;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class runSlave extends run {

    // Data
    private int[] hostTank = {50, 340, 4}; // [x,y,d]
    private int[] clientTank = {900, 340, 12}; // [x,y,d]
    private boolean quitKey = false; // true when the quit key is pressed
    private boolean quit = false;
    private Image bulletImage;
    private ArrayList<Integer> bulletsX = new ArrayList<>();
    private ArrayList<Integer> bulletsY = new ArrayList<>();

    // socket variables
    private Socket socket;
    private BufferedReader inReader;
    private PrintWriter outWriter;
    private String inputString;
    private mpControllerClient controller;

    public runSlave (Socket socket, BufferedReader inReader, PrintWriter outWriter,
                     mpControllerClient controller) {
        this.socket = socket;
        this.inReader = inReader;
        this.outWriter = outWriter;
        this.controller = controller;
    }

////////////////////////////////////GAME LOOP

    protected void gameProcessing() {

        if (quit) {
            Quit();

        } else if (time < 0) { // pre masterTanks

            gc.clearRect(0, 0, 1024, 768);
            renderSprites();
            renderCountdown();

        } else if (time < gameTime) { // in game

            // object processing
            tankProcessing();

            gc.clearRect(0, 0, 1024, 768);
            renderScore();
            renderBullets();
            renderSprites();
            renderCountdown();
        }
    }

////////////////////////////////////PARALLEL LOOP

    protected void startParallelLoop() {
        processingLoop = new parallelSocket();
        processingLoop.setPeriod(Duration.seconds(secondsPerFrame));
        processingLoop.setOnFailed(t -> Quit());
        processingLoop.start();
    }
    private class parallelSocket extends parallel {
        @Override protected void parallelProcessing() {
            //System.out.println("run socket "+time); System.out.flush();

            // data transfer
            try {
                // send positions and time, receive keyboard inputs
                onlineDataTransfer();
            } catch (IOException e) {
                controller.setStatusDisplay("In-Game Error: Connection Broken");
                Platform.runLater(() -> Quit());
                e.printStackTrace();
            }
        }
    }

////////////////////////////////////INITIALIZING

    protected void createGameObjects() {
        masterTanks.getStage().setTitle( "Master Tanks Client" );

        tank1 = new spriteTank(clientTank[0], clientTank[1], masterTanks.getTank(), clientTank[2]);
        tank1Controls = new String[] {"W", "A", "S", "D", "SPACE"};
        tank2 = new spriteTank(hostTank[0], hostTank[1], 4, hostTank[2]);

        bulletImage = new Image(bulletURL);

        // not used but required to prevent rendering errors (see renderSprites in run)
        powerUp = new spritePowerUp();

        makeWalls1();
    }
    protected void onlineSynch() throws IOException {
        // The initial synchronization

        // read
        inputString = inReader.readLine();
        System.out.println(inputString); // Host ready!

        // write
        outWriter.println("Client ready!");

        recieveData();
        sendData();
    }

////////////////////////////////////ONLINE DATA TRANSFER

    private void onlineDataTransfer() throws IOException {
        // send keyboard inputs, receive positions and time

        recieveData();
        sendData();
    }
    private void recieveData() throws IOException {
        /*
        String gameData = "Data\n" +
                "50,340,4\n" +    // Host Tank [x,y,d]
                "900,340,12\n" +  // Client Tank [x,y,d]
                "0\n" +           // Time
                "10,20,300\n" +   // Bullet x positions
                "30,20,120\n" +   // Bullet y positions
                "false";          // Quit = true or false
        */
        inputString = inReader.readLine(); // Data
        if (!inputString.equals("Data")) { // should equal 'Data', otherwise an error has occured
            System.out.println("Error: Corrupted data recieved");
            //Quit(); TODO exception handelling?
        }

        inputString = inReader.readLine();// Host Tank [x,y,d]
        int lastComma = 0;
        int arrayIndex = 0;
        char c;
        for (int i = 0; i < inputString.length(); i++) {
            c = inputString.charAt(i);
            if (c == ',') {
                hostTank[arrayIndex] = Integer.parseInt(inputString.substring(lastComma, i));
                lastComma = i + 1;
                arrayIndex++;
            }
        }

        inputString = inReader.readLine();// Client Tank [x,y,d]
        lastComma = 0;
        arrayIndex = 0;
        for (int i = 0; i < inputString.length(); i++) {
            c = inputString.charAt(i);
            if (c == ',') {
                clientTank[arrayIndex] = Integer.parseInt(inputString.substring(lastComma, i));
                lastComma = i + 1;
                arrayIndex++;
            }
        }

        inputString = inReader.readLine();// Time
        time = Integer.parseInt(inputString);

        inputString = inReader.readLine();// Bullet x positions = x,x,x,\n OR null
        lastComma = 0;
        bulletsX.clear();
        if (!inputString.equals("null")) {
            for (int i = 0; i < inputString.length(); i++) {
                c = inputString.charAt(i);
                if (c == ',') {
                    bulletsX.add(Integer.parseInt(inputString.substring(lastComma, i)));
                    lastComma = i + 1;
                }
            }
        }

        inputString = inReader.readLine();// Bullet y positions = y,y,y,\n OR null
        lastComma = 0;
        bulletsY.clear();
        if (!inputString.equals("null")) {
            for (int i = 0; i < inputString.length(); i++) {
                c = inputString.charAt(i);
                if (c == ',') {
                    bulletsY.add(Integer.parseInt(inputString.substring(lastComma, i)));
                    lastComma = i + 1;
                }
            }
        }

        inputString = inReader.readLine();// Quit = true or false
        if (inputString.equals("true"))
            quit = true;

    }
    private void sendData() {
        /*
        String gameData = "Data\n" +
                "01001\n" +    // Player Inputs [W,A,S,D,SPACE]
                "false";       // Quit = true or false
        */
        String gameData = "Data\n";     // Data

        for (int i = 0; i < 5; i++) {   // W A S D SPACE
            if (activeKeys.contains(tank1Controls[i]))
                gameData += 1;
            else
                gameData += 0;
        } gameData += "\n";

        if(quitKey) {                   // Quit = true or false
            gameData += "true";
            quit = true;
        } else {
            gameData += "false";
        }

        outWriter.println(gameData);
    }

////////////////////////////////////SPRITE PROCESSING

    private void tankProcessing() {
        tank1.setPosition(clientTank[0], clientTank[1], clientTank[2]);
        tank2.setPosition(hostTank[0], hostTank[1], hostTank[2]);
    }
    private void renderBullets() {
        for (int i = 0; i < bulletsX.size(); i++) {
            gc.drawImage( bulletImage, bulletsX.get(i), bulletsY.get(i) );
        }
    }

////////////////////////////////////INPUT HANDLING

    protected void prepareActionHandlers() {
        gameScene.setOnKeyPressed(event -> { // when a key is released...
            if (event.getCode().toString().equals("ESCAPE")) {
                quitKey = true;
            }
            if (!activeKeys.contains(event.getCode().toString())) {
                // write to log file
                Date date= new Date();
                logFile.printf("%-25s %s%n", new Timestamp(date.getTime()), event.getCode().toString());
                activeKeys.add(event.getCode().toString()); // it is added to the list
            }
        });
        gameScene.setOnKeyReleased(event -> { // when a key is released...
            activeKeys.remove(event.getCode().toString()); // it is added to the list
        });
    }

///////////////////////////////////QUIT

    @Override protected void Quit() {
        // stop the game and processing loops
        processingLoop.cancel();
        gameLoop.stop();

        masterTanks.getStage().setTitle( "Master Tanks bruh" );
        System.out.println("Slave done.");
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            logFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // stop game music and masterTanks menu music
        if (enableMusic)
            musicPlayer.play("menu");

        masterTanks.getMultiplayer().setClientScene(); // go back to slave page
    }
}
