/*
https://www.youtube.com/watch?v=-L-WgKMFuhE
http://www.jgallant.com/nodal-pathfinding-in-unity-2d-with-a-in-non-grid-based-games/
щ(ಠ益ಠщ)
 */

package csmasters.runtime;

import csmasters.setup.config;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.ArrayList;

class aiAStar {
    private ArrayList<aiNode> nodesAll = new ArrayList<>(); // all nodes in valid moving positions
    private ArrayList<aiNode> nodesOpen = new ArrayList<>(); // discovered nodes, to be evaluated
    private ArrayList<aiNode> nodesClosed = new ArrayList<>(); // evaluated nodes
    private aiNode target = null;
    private Boolean targetReached = false;

//////////////////////////////////// INITIALIZATION

    aiAStar(ArrayList<spriteWall> walls, double tankSize) {
        // raycast map to get all nodes
        addNodes();

        // remove nodes that are close to walls
        removeNodes(walls, tankSize);
    }
    private void addNodes() {
        // create a matrix of nodes at intervals of nodeSize across the screen
        // and initalize the nodes with references to adjacent nodes
        int x = config.nodeSize; // where to put the next node
        int y = config.nodeSize;
        int rowSize = config.xCanvasSize / config.nodeSize; // number of nodes in a row
        int rowIndex = 0; int colIndex = 0; // row and column indexes of the node matrix

        while ( y < config.yCanvasSize ) {
            colIndex = 0;
            while ( x < config.xCanvasSize ) {
                // add a new node to the arrayList
                aiNode newNode = new aiNode(x, y, this);
                nodesAll.add(newNode);

                // update the neighbours of the new node
                if (rowIndex != 0) {
                    // top nodes
                    if (colIndex != 0) {
                        // top left node (direction = 7)
                        newNode.addAdjacent(7, nodesAll.get((rowIndex-1)*rowSize + colIndex-1));
                    }
                    if (colIndex < rowSize-1) {
                        // top right node (direction = 1)
                        newNode.addAdjacent(1, nodesAll.get((rowIndex-1)*rowSize + colIndex+1));
                    }
                    // top node (direction = 0)
                    newNode.addAdjacent(0, nodesAll.get((rowIndex-1)*rowSize + colIndex));
                }
                if (colIndex != 0) {
                    // left node (direction = 6)
                    newNode.addAdjacent(6, nodesAll.get(rowIndex*rowSize + colIndex-1));
                }

                // incriment position for next node
                colIndex++;
                x += config.nodeSize;
            }
            rowIndex++;
            x = config.nodeSize;
            y += config.nodeSize;
        }
    }
    private void removeNodes(ArrayList<spriteWall> walls, double tankSize) {
        // remove nodes that are close to walls
        int i = 0;
        while (i < nodesAll.size()) {
            for (spriteWall wall : walls) {
                aiNode node = nodesAll.get(i);
                if (wall.nodeProximity(node.getPos()[0], node.getPos()[1], tankSize*config.wallProximityFactor)) {
                    node.removeAdjacents();
                    nodesAll.remove(i);
                    i--; // to counteract the i++;
                    break; // for efficiency
                }
            }
            i++;
        }
    }

//////////////////////////////////// IO

    void renderNodes(GraphicsContext gc) {
        for (aiNode node: nodesAll) {
            node.draw(gc);
        }
        if (target != null) {
            gc.setFill(Color.rgb(0,0,255));
            gc.fillRect(target.getPos()[0], target.getPos()[1], 3, 3);
        }
    }
    ArrayList<aiNode> getNodesOpen() { return nodesOpen; }
    ArrayList<aiNode> getNodesClosed() { return nodesClosed; }
    aiNode getTarget() { return target; }

//////////////////////////////////// PROCESSING

    Boolean process(sprite tankSelf, sprite tankTarget) {
        // reset all nodes from the last scan
        resetNodes();

        // the closest node to tankTarget is the target
        target = findClosest(tankTarget);
        target.setTarget();

        // the closest node to tankSelf is the masterTanks
        aiNode start = findClosest(tankSelf);
        start.discoverStart();

        // evaluate masterTanks
        targetReached = start.evaluate();

        aiNode previousNode = new aiNode(0,0,this);// TODO: debugging

        // evaluate open nodes until target is reached
        while (!targetReached) {
            if (nodesOpen.size() == 0) {
                System.out.println("A* PATHPLANNING ERROR: NO OPEN NODES, TARGET UNREACHABLE [aiAStar->target]");
                return false; // result = failure
            }

            // find the open node with the smallest F
            aiNode nextNode = nodesOpen.get(0);
            for (aiNode node: nodesOpen) {
                if (node.getF() < nextNode.getF())
                    nextNode = node;
                else if (node.getF() == nextNode.getF() && node.getH() < nextNode.getH())
                    nextNode = node; // prioritise node closest to target when multiple nodes have the same F
            }

            if (nextNode == previousNode) {
                System.out.println("duplicate!!!");
            }
            previousNode = nextNode;

            // evaluate the node in nodesOpen with the smallest F
            // check if the end is reached
            targetReached = nextNode.evaluate();
        }

        return true; // result = success
    }
    int[] target() {
        // sets the target to the next node
        // must be called after process has been run
        if (!targetReached)
            return new int[]{-1,-1};

        // get next node for tank to drive to
        aiNode nextNode = target.nodeGoTo();

        // return target coordinates for tank to drive to
        return nextNode.getPos();
    }
    int[] targetSmoothed() {
        // must be called after process has been run
        if (!targetReached)
            return new int[]{-1,-1};

        // get next node for tank to drive to
        aiNode nextNode = target.nodeGoToSmoothed();

        // return target coordinates for tank to drive to
        return nextNode.getPos();
    }

    private void resetNodes() {
        // reset all nodes from the last scan
        while (nodesClosed.size() > 0) {
            nodesClosed.get(0).resetNode();
            nodesClosed.remove(0);
        } while (nodesOpen.size() > 0) {
            nodesOpen.get(0).resetNode();
            nodesOpen.remove(0);
        }
    }
    private aiNode findClosest(sprite tank) {
        aiNode closest = nodesAll.get(0); // the closest node to tank
        double tankX = tank.getCenterX();
        double tankY = tank.getCenterY();
        double vx = tankX - closest.getPos()[0], vy = tankY - closest.getPos()[1];
        double distance = Math.sqrt( vx*vx + vy*vy ); // distance from tank to closest node
        double distanceTemp;

        for (aiNode node: nodesAll) {
            vx = tankX - node.getPos()[0]; vy = tankY - node.getPos()[1];
            distanceTemp = Math.sqrt( vx*vx + vy*vy );
            if (distanceTemp < distance) {
                closest = node;
                distance = distanceTemp;
            }
        }
        return closest;
    }
}
