package csmasters.setup;

////////////////// OPTIMISED FOR THE TANK OPTIONS SCREEN

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class animationTanks extends Transition {

    private final ImageView imageView;
    private Image[] tanks = new Image[4];
    private Image[] tanksAlt = new Image[4];
    private Image panda;
    private int tankNum = 0;

    animationTanks(ImageView imageView, int tankNum) {
        System.out.println("loading animation");
        String URL = getClass().getResource( config.pandaAnimationURL ).toExternalForm();
        System.out.println(URL);
        panda = new Image(URL, 192*4, 48*4, true, false);
        this.imageView = imageView;

        System.out.println("loading tankURL");
        URL = getClass().getResource( config.tankURL ).toExternalForm();
        URL = URL.substring(0,URL.length()-5);
        System.out.println(URL);
        for (int i = 0; i < 4; i++) {
            tanks[i] = new Image(URL + i + ".png", 48*4, 48*4, true, false);
            tanksAlt[i] = new Image(URL + i + "alt.png", 48*4, 48*4, true, false);
        }
        setTankImage(tankNum);
        setInterpolator(Interpolator.LINEAR);
    }

    void setTankImage(int tankNum) {
        this.tankNum = tankNum;
        imageView.setViewport(new Rectangle2D(0, 0, 48*4, 48*4));
        if (tankNum == 0) {
            setCycleDuration(Duration.millis(400));
        } else {
            setCycleDuration(Duration.millis(200));
        }
    }

    protected void interpolate(double k) {
        if (tankNum == 0) {
            imageView.setImage(panda);
            int index = (int)(k*4);
            imageView.setViewport(new Rectangle2D(48*4*index, 0, 48*4, 48*4));
        } else {
            if ((int)(k*2)%2 == 0) {
                imageView.setImage(tanks[tankNum]);
            } else {
                imageView.setImage(tanksAlt[tankNum]);
            }
        }
    }
}