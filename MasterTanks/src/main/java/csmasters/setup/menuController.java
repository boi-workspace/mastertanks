package csmasters.setup;

import csmasters.runtime.runOffline;

import javafx.animation.Animation;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class menuController {

////////////////////////// MAIN MENU

    @FXML
    public StackPane title;
    @FXML
    public void initialize() {
        // set up logo
        ImageView logoViewer = new ImageView();
        String logoUrl = getClass().getResource( config.logoAnimURL ).toExternalForm();
        animation logoAnimation = new animation(logoViewer, logoUrl, 180, 42, 100, 6, 1, 5);
        logoAnimation.setCycleCount(Animation.INDEFINITE);
        logoAnimation.play();
        title.getChildren().add(logoViewer);
    }

    @FXML
    public void gotoTraning() {
        masterTanks.gameMode = 0;
        startGame();
    }
    @FXML
    public void gotoSinglePlayer() {
        masterTanks.gameMode = 1;
        startGame();
    }
    @FXML
    public void gotoLocal() {
        masterTanks.gameMode = 2;
        startGame();
    }
    @FXML
    public void gotoOnline() { masterTanks.getMultiplayer().setMultiplayerScene(); }
    @FXML
    public void gotoOptions() {
        masterTanks.getStage().setScene(masterTanks.getOptionsScene());
    }

    private void startGame() {
        runOffline game = new runOffline();
        game.gameStart(masterTanks.getStage());
    }

////////////////////////// TODO: GAME OVER SCREEN

}
