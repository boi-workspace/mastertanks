package csmasters.setup;

import javafx.application.Platform;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

public abstract class musicPlayer implements config {
    private static Clip playing = null;
    private static URL[] links;
    private static final int numSongs = 2;

    static void initPlayer() {
        // initialize
        links = new URL[numSongs];
        links[0] = musicPlayer.class.getResource(menuSongURL);
        links[1] = musicPlayer.class.getResource(gameSongURL);
    }
    static void playIntro() {
        startSong(musicPlayer.class.getResource(introSongURL), false);
        LineListener listener = event -> {
            // only continue if at the end of the song
            if (event.getType() != LineEvent.Type.STOP)
                return;

            stop();
            // play next song
            if (enableMusic)
                startSong(links[0],true);

            // transition from intro scene to menu
            Platform.runLater(masterTanks::enterGame);
            };
        playing.addLineListener(listener);
    }

    public static void play(String song) {
        // if a song is playing, it's stopped
        stop();

        int songNum;
        switch (song) {
            case "menu":
                songNum = 0; break;
            case "game":
                songNum = 1; break;
            default:
                System.out.println("MUSIC PLAYER ERROR: invalid song name. musicPlayer -> play()");
                return;
        }
        if ( songNum < 0 || numSongs <= songNum ) {
            System.out.println("MUSIC PLAYER ERROR: invalid song number. musicPlayer -> play()");
            return;
        }

        // play new song
        startSong(links[songNum], true);
    }
    private static void startSong(URL url, boolean loop) {
        try {
            // Open an audio input stream.
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            // Get a sound clip resource.
            playing = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            playing.open(audioIn);
            if (loop)
                playing.loop(Clip.LOOP_CONTINUOUSLY);
            else
                playing.loop(0);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }
    private static void stop() {
        // if a song is playing, it's stopped
        if (playing != null) {
            // stop
            playing.stop();
        }
    }
}