package csmasters.setup;

////////////////// FLEXIBLE FRAMEWORK FOR SPRITESHEETS

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class animation extends Transition {

    private final ImageView imageView;
    private int rows, cols, width, height;
    private int row = 0, column = 0;

    animation(ImageView imageView, String URL, int width, int height,
              int millisPeriod, int cols, int rows, int scaleFactor) {
        this.rows = rows;
        this.cols = cols;
        this.width = width*scaleFactor;
        this.height = height*scaleFactor;
        this.imageView = imageView;

        imageView.setImage(new Image(URL, cols*width*scaleFactor,
                rows*height*scaleFactor, true, false));
        imageView.setViewport(new Rectangle2D(width*column, height*row, width, height));

        setCycleDuration(Duration.millis(millisPeriod*cols));
        setInterpolator(Interpolator.LINEAR);
    }
    void setRow(int row) {
        if (0 <= row && row < rows) {
            this.row = row;
            imageView.setViewport(new Rectangle2D(width*column, height*row, width, height));
        } else {
            System.out.println("ANIMATION ERROR: invalid row. animation -> setRow()");
        }
    }
    protected void interpolate(double k) {
        column = (int) (k * cols);
        imageView.setViewport(new Rectangle2D(width*column, height*row, width, height));
    }
}
