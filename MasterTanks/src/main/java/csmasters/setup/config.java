package csmasters.setup;

// Configuration file
// This file contains variables that modify game settings
// TODO: change time values to vary with the fps

public interface config {

////////////////////////// CONSTANT VALUES

	// Screen resolution
	int xResolution = 1000; // 1024
	int yResolution = 740; // 768
	int xOffset = 8;
	int yOffset = 31;
	int xCanvasSize = xResolution - xOffset*2;
	int yCanvasSize = yResolution - yOffset - xOffset;

	// menu constants
	int numMaps = 3; // the number of availible maps
	int numTanks = 4; // the number of tanks for the player to choose from

	// game constants
	Boolean enableIntro = false; // enables the intro
	Boolean enableMusic = false; // enables music
	Boolean debuggingMode = true; // adds data to canvas in game
	Boolean aiTesting = false; // for testing A* functionality with and aiBoi sprite
	int nodeSize = 30;
	double wallProximityFactor = 0.7; // multiplied by tankSize for a distance away from walls to remove nodes
	int rotateIncrements = 16; // 360/this = the angle that the tank rotates per increment (16)

	// music
	String menuSongURL = "/assets/mus/come_on_SS.wav";
	String introSongURL = "/assets/mus/come_on_intro.wav";
	String gameSongURL = "/assets/mus/hot_toad.wav";
	String shootSoundURL = "/assets/mus/Tim Allen's Home Improvement Grunt.mp3";

	// menu assets
	String logoURL = "/assets/logo.png";
	String backgroundURL = "/assets/menu_background.png";
	String fireURL = "/assets/fire_spritesheet.png";
	String logoAnimURL = "/assets/logo_animation.png";
	String pandaAnimationURL = "/assets/panda_sprites/panda_anim.png";
	String mapPreviewURL = "/assets/map0.png"; // + num

	String titleFontURL = "/assets/warmongerbb.ttf"; // TODO: unused
	String textFontURL = "/assets/SFIntermosaic.ttf";

	// game asset
	String gameBackgroundURL = "/assets/game_background.png";
	String bulletURL = "/assets/bullet.png";
	String pandaURL = "/assets/panda_sprites/panda_walk_0.png"; // + num
	String pandaShootURL = "/assets/panda_sprites/panda_shoot_0.png"; // + num
	String tankURL = "/assets/tank_sprites/tank0.png"; // + num
	String wallURL = "/assets/wallImage1.png"; // + num
	String powerUpURL = "/assets/powerUp.png";

	// layout files
	String mainMenuURL = "/layout/mainMenuLayout.fxml";
	String optionsURL = "/layout/optionsLayout.fxml";
	String mpURL = "/layout/mpLayout.fxml";
	String mpHostURL = "/layout/mpLayoutHost.fxml";
	String mpClientURL = "/layout/mpLayoutClient.fxml";

	double secondsPerFrame = 0.0333; // the inverse of the desired fps (currently 60fps)
	// time constants
	int gameTime = 1800; // the number of frames that a cgame will last for 1800
	int tankSpeed = 6; // 6
	int shootDelay = 10; // the number of frames between each bullet shot
	int bulletSpeed = 8; // 16
	int bulletLifespan = 80; // the number of frames the bullet will exist for
	int rotateDelay = 3; // the number of frames before each increment of the tank's rotation (3)
	int tankInvun = 30; // number of frames that the tank is invulnerable after spawning
	int powerUpLifespan = 400; // frames before power up sprite disappears
}
