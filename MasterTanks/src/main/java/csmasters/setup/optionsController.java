package csmasters.setup;

import csmasters.runtime.aiBrain;

import javafx.animation.Animation;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class optionsController {

////////////////////////// OPTIONS MENU

    @FXML
    public Label title;
    @FXML
    public ListView optionsList;
    @FXML
    public VBox detailsBox;
    @FXML
    public Button back;
    @FXML
    public ImageView logo;

    private ImageView difficultyView, tankView, mapView;
    private animation difficultyDisplay;
    private animationTanks tankDisplay;
    private Label nameDisplay;

    @FXML
    public void initialize() {
        System.out.println("loading options");

        difficultyView = new ImageView();
        tankView = new ImageView();
        mapView = new ImageView();
        nameDisplay = new Label();

        System.out.println("loading animationTanks");
        tankDisplay = new animationTanks(tankView, masterTanks.Tank);

        System.out.println("loading fireURL");
        String URL = getClass().getResource( config.fireURL ).toExternalForm();
        System.out.println(URL);
        difficultyDisplay = new animation(difficultyView, URL, 48, 48, 100, 4, 3, 4);

        System.out.println("loading logoURL");
        URL = getClass().getResource( config.logoURL ).toExternalForm();
        System.out.println(URL);
        logo.setImage(new Image(URL, 900, 210, true, false));
    }

    @FXML
    public void gotoMain() {
        detailsBox.getChildren().clear();
        difficultyDisplay.stop();
        tankDisplay.stop();
        masterTanks.getStage().setScene(masterTanks.getStartScene());
    }

    @FXML
    public void BMO_CHOP() {
        detailsBox.getChildren().clear();

        switch (optionsList.getSelectionModel().getSelectedItem().toString()) {
            case "Tank Skin":
                tankSetup();
                break;
            case "Difficulty":
                difficultySetup();
                break;
            case "Controls":
                controlsSetup();
                break;
            case "Map":
                mapSetup();
                break;
        }
    }

    private void tankSetup() {
        // buttons
        Button left = new Button(" < ");
        left.getStyleClass().add("options-button");
        left.setOnAction(event -> tankButtonLeft());
        Button right = new Button(" > ");
        right.getStyleClass().add("options-button");
        right.setOnAction(event -> tankButtonRight());

        // tank name
        nameDisplay.getStyleClass().add("description");

        // hbox for buttons and name
        Region gap1 = new Region(); Region gap2 = new Region();
        HBox selection = new HBox();
        selection.getChildren().addAll(left,gap1,nameDisplay,gap2,right);
        HBox.setHgrow(gap1,Priority.ALWAYS);
        HBox.setHgrow(gap2,Priority.ALWAYS);

        // vbox for animation and hbox
        detailsBox.getChildren().add(tankView); // top
        detailsBox.getChildren().add(selection); // bottom
        detailsBox.setAlignment(Pos.TOP_CENTER);
        detailsBox.setPadding(new Insets(15)); // outside box
        detailsBox.setSpacing(15); // inbetween elements

        // tank image display
        tankDisplay.setCycleCount(Animation.INDEFINITE);
        updateTank(); // starts the tank animation in tankView
    }
    private void tankButtonLeft() {
        masterTanks.Tank--;
        masterTanks.Tank = (masterTanks.Tank + config.numTanks) % config.numTanks;
        updateTank();
    }
    private void tankButtonRight() {
        masterTanks.Tank++;
        masterTanks.Tank = masterTanks.Tank % config.numTanks;
        updateTank();
    }
    private void updateTank() {
        switch(masterTanks.Tank) {
            case 0:
                nameDisplay.setText("Panda!"); break;
            case 1:
                nameDisplay.setText("Invisible!"); break;
            case 2:
                nameDisplay.setText("Fab!"); break;
            case 3:
                nameDisplay.setText("Cheap!"); break;
            default:
                nameDisplay.setText("Panda!");
        }
        tankDisplay.stop();
        tankDisplay.setTankImage(masterTanks.Tank);
        tankDisplay.play();
    }

    private void difficultySetup() {
        Label title = new Label("AI Difficulty");
        title.getStyleClass().add("description");

        // buttons
        Button left = new Button(" < ");
        left.getStyleClass().add("options-button");
        left.setOnAction(event -> difficultyButtonLeft());
        Button right = new Button(" > ");
        right.getStyleClass().add("options-button");
        right.setOnAction(event -> difficultyButtonRight());

        // difficulty level - easy, normal, hard
        nameDisplay.getStyleClass().add("description");

        // hbox for buttons and name
        Region gap1 = new Region(); Region gap2 = new Region();
        HBox selection = new HBox();
        selection.getChildren().addAll(left,gap1,nameDisplay,gap2,right);
        selection.setPadding(new Insets(20));
        HBox.setHgrow(gap1,Priority.ALWAYS);
        HBox.setHgrow(gap2,Priority.ALWAYS);

        // vbox for title, animation and hbox
        detailsBox.getChildren().add(title);
        detailsBox.getChildren().add(difficultyView);
        detailsBox.getChildren().add(selection);
        detailsBox.setAlignment(Pos.TOP_CENTER);
        detailsBox.setPadding(new Insets(10)); // outside box
        detailsBox.setSpacing(10); // inbetween elements

        // masterTanks animation
        difficultyDisplay.setCycleCount(Animation.INDEFINITE);
        updatDifficulty();
    }
    private void difficultyButtonLeft() {
        masterTanks.AIDifficulty--;
        masterTanks.AIDifficulty = (masterTanks.AIDifficulty + 3) % 3;
        updatDifficulty();
    }
    private void difficultyButtonRight() {
        masterTanks.AIDifficulty++;
        masterTanks.AIDifficulty = masterTanks.AIDifficulty % 3;
        updatDifficulty();
    }
    private void updatDifficulty() {
        aiBrain.setDifficulty(masterTanks.AIDifficulty);
        switch(masterTanks.AIDifficulty) {
            case 0:
                nameDisplay.setText("Easy"); break;
            case 1:
                nameDisplay.setText("Normal"); break;
            case 2:
                nameDisplay.setText("Hard"); break;
            default:
                nameDisplay.setText("Panda!");
        }
        difficultyDisplay.setRow(masterTanks.AIDifficulty);
        difficultyDisplay.play();
    }

    private void controlsSetup() {

        Label title = new Label("In-Game Controls:");
        title.getStyleClass().add("description");

        Label controls = new Label("Player 1 Movement:\n- W,A,S,D\nPlayer 1 shoot:\n- SPACE\n"+
                "Player 2 Movement:\n- NumPad 8,4,5,6\nPlayer 2 shoot:\n- NumPad 0\n"+
                "Stop Game: ESC");
        controls.getStyleClass().add("controls_description");

        // vbox for image and hbox
        detailsBox.getChildren().add(title);
        detailsBox.getChildren().add(controls);
        detailsBox.setAlignment(Pos.TOP_LEFT);
        detailsBox.setPadding(new Insets(15)); // outside box
        detailsBox.setSpacing(15); // inbetween elements
    }

    private void mapSetup() {
        // buttons
        Button left = new Button(" < ");
        left.getStyleClass().add("options-button");
        left.setOnAction(event -> mapButtonLeft());
        Button right = new Button(" > ");
        right.getStyleClass().add("options-button");
        right.setOnAction(event -> mapButtonRight());

        // map title
        nameDisplay.getStyleClass().add("description");

        // hbox for buttons and name
        Region gap1 = new Region(); Region gap2 = new Region();
        HBox selection = new HBox();
        selection.getChildren().addAll(left,gap1,nameDisplay,gap2,right);
        selection.setPadding(new Insets(20));
        HBox.setHgrow(gap1,Priority.ALWAYS);
        HBox.setHgrow(gap2,Priority.ALWAYS);

        // vbox for image and hbox
        detailsBox.getChildren().add(mapView);
        detailsBox.getChildren().add(selection);
        detailsBox.setAlignment(Pos.TOP_CENTER);
        detailsBox.setPadding(new Insets(15)); // outside box
        detailsBox.setSpacing(15); // inbetween elements

        updateMap(); // sets the image and text
    }
    private void mapButtonLeft() {
        masterTanks.Map--;
        masterTanks.Map = (masterTanks.Map + config.numMaps) % config.numMaps;
        updateMap();
    }
    private void mapButtonRight() {
        masterTanks.Map++;
        masterTanks.Map = masterTanks.Map % config.numMaps;
        updateMap();
    }
    private void updateMap() {
        switch (masterTanks.Map) {
            case 0:
                nameDisplay.setText("Empty\nMap!");
                break;
            case 1:
                nameDisplay.setText("Map 1!");
                break;
            case 2:
                nameDisplay.setText("Map 2!");
                break;
            default:
                nameDisplay.setText("Panda!");
        }
        String URL = getClass().getResource( config.mapPreviewURL ).toExternalForm();
        URL = URL.substring(0,URL.length()-5);
        mapView.setImage(new Image(URL + masterTanks.Map + ".png"));
    }
}
