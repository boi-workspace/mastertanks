/*
Authors: David Hamill and Ethan Yee

This class is the main one, it does pretty much everything outside of the gameplay,
mainly handling the menus and transitions.
 */

package csmasters.setup;

import csmasters.multiplayer.multiplayerSetup;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.net.URL;

import java.io.IOException;

public class masterTanks extends Application implements config {

////////////////////////// VARIABLES

	private static Stage mainStage;
	private static Scene startScene, optionsScene;
	private static Font buttonFont, textFont;

	// multiplayer data
	private static multiplayerSetup multiplayer;

	// game setup
	static int Tank = 0; // which tank is chosen
	static int AIDifficulty = 1; // 0 = easy, 1 = normal, 2 = hard
	static int Map = 1; // which map is chosen, 0 is no walls
	static int gameMode = 0; // Game mode (training - 0, single player - 1, local multiplayer - 2)

////////////////////////// GET FUNCTIONS

	public static Stage getStage() {return mainStage;}
	public static Scene getStartScene() {return startScene;}
	static Scene getOptionsScene() {return optionsScene;}
	public static multiplayerSetup getMultiplayer() {return multiplayer;}

	public static Font getTextFont() {return textFont;}
	public static Font getButtonFont() {return buttonFont;}

	public static int getGameMode() {return gameMode;}
	public static int getMap() {return Map;}
	public static int getTank() {return Tank;}

////////////////////////// START BOIII

	public static void main(String[] args) { launch(args); }

    @Override
    public void start(Stage theStage) {

		mainStage = theStage;
    	setUp();

    	if (enableIntro) {
			// go to intro scene
			introScreen();
			musicPlayer.playIntro();
		} else {
			theStage.setScene(startScene);
			theStage.show();
			if (enableMusic)
				musicPlayer.play("menu");
		}
    }

////////////////////////// SET UP THE WINDOW AND ASSOCIATED OBJECTS

    public void setUp() {

		//Load in fonts
		textFont = Font.loadFont(getClass().getResourceAsStream( textFontURL ), 22);
		buttonFont = Font.loadFont(getClass().getResourceAsStream( textFontURL ), 35);

    	//Create stage and root
		mainStage.setWidth(xResolution);
		mainStage.setHeight(yResolution);
		mainStage.setResizable(false);
		mainStage.setTitle( "Master Tanks bruh" );

		//Set up the masterTanks menu
		try {
			// background image
			String backurl = getClass().getResource("/assets/menu_background.png").toExternalForm();
			System.out.println("---***--- " + backurl);

			// load start scene
			URL url = getClass().getResource( mainMenuURL );
			System.out.println("---***--- " + url);
			Parent root = FXMLLoader.load(url);
			startScene = new Scene(root);
			root.setStyle("-fx-background-image: url(\""+backurl+"\");");

			// load css
			String cssUrl = getClass().getResource("/layout/menu.css").toExternalForm();
			startScene.getStylesheets().add(cssUrl);

			// load options scene
			url = getClass().getResource( optionsURL );
			System.out.println("---***--- " + url);
			root = FXMLLoader.load(url);
			optionsScene = new Scene(root);
			root.setStyle("-fx-background-image: url(\""+backurl+"\");");
			optionsScene.getStylesheets().add(cssUrl);


		} catch (Exception e) {
			e.printStackTrace();
		}

        // Set up multiplayer menu
		multiplayer = new multiplayerSetup(mainStage, startScene);

		musicPlayer.initPlayer();
    }

////////////////////////// INTRO SCREEN

	private static int keyCount = 0;
	private static Timeline gameLoop = new Timeline();
	private static void introScreen() {
		Group root = new Group();
		Scene introScene = new Scene(root);
		introScene.setFill(Color.rgb(0,0,0));

		gameLoop = new Timeline();

		KeyFrame kf = new KeyFrame(
				Duration.millis(1000/30),
				ae -> { // event handler
					keyCount++;
					double color = (double)keyCount/80;
					color = 255*color*color;
					if (color > 255)
						color = 0;
					introScene.setFill(Color.rgb((int)color,(int)color,(int)color));
				});

		gameLoop.setCycleCount( 60*3 ); // 3 seconds
		gameLoop.getKeyFrames().add( kf );
		gameLoop.setOnFinished(ae -> {
				System.out.println("INTRO SCENE ERROR: timeline finished before song. masterTanks -> introScreen() & musicPlayer -> playIntro();");
				enterGame();}); // in case song doesn't work
		gameLoop.play();

		mainStage.setScene(introScene);
		mainStage.show();
	}
	static void enterGame() {
		if (!gameLoop.statusProperty().equals(Animation.Status.STOPPED))
			gameLoop.stop();
		mainStage.setScene(startScene);
	}
}

