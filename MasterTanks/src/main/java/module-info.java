module csmasters {

    exports csmasters.multiplayer;
    exports csmasters.runtime;
    exports csmasters.setup;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.media;

    requires java.sql;
    requires java.desktop;
}