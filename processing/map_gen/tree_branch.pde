/*
TREE BRANCH
use treeBranchUpdate() in setup, treeBranch() and drawBranch() in draw
TODO: clear outer zone (no branches reach the border)
*/

////////// CONFIG

final int change1 = 30; // chance of direction change +- 1
final int change2 = 3; // chance of direction change +- 2
final float radius = 96; // size of gap between walls
final boolean wallProximity = false; // if enabled, walls cannot exist within the defined radius of eachother

int seedx,seedy,seedi,seedj,direction;
ArrayList<int[]> branch = new ArrayList<int[]>(); // where the latest branch is stored. added to cells at completion
color branchColor = color(0,204,90);

////////// INITIALIZATION { functions called by treeBranchUpdate() }

void treeBranchUpdate() {
  if (inRange(seedi,0,width/cellSize) && inRange(seedj,0,height/cellSize)
        && (!wallProximity || !inProximity(seedi,seedj))) {
    branch.add(new int[]{seedi,seedj});
  } else {
    treeBranchInit();
  }
}
private void treeBranchInit() {
  // initialize a new seed
  seedx = (int)random(0.2*width,0.8*width);
  seedy = (int)random(0.2*height,0.8*height);
  seedi = seedx/cellSize;
  seedj = seedy/cellSize;
  direction = (int)random(8);
  // load and reset the branch array list
  loadBranch();
  //treeBranchUpdate(); // TODO: recursive loop will crash the program
}
private void loadBranch() {
  for (int[] pos: branch) {
    cells[pos[0]][pos[1]] = true;
  }
  branch.clear();
}

////////// ITTERATION { functions called by treeBranch() }

void treeBranch() {
  delay(500);
  
  // determine new direction
  float dice = random(100);
  if (dice < change2)
    direction-=2;
  else if (dice < change1+change2)
    direction--;
  else if (100-change2 < dice)
    direction+=2;
  else if (100-change1-change2 < dice)
    direction++;
  // chance of direction remaining the same = 100 - 2*change1 - 2*change2
  direction = (direction+8)%8;
  
  // add a new element
  if (direction%4 != 0) {
    if (direction < 4)
      seedi++;
    else
      seedi--;
  }
  if ((direction+2)%4 != 0) {
    if (6 < direction && direction < 2)
      seedj++;
    else
      seedj--;
  }
  treeBranchUpdate();
}
private boolean inProximity(int ii, int jj) {
  // returns true if the index is in the radius of another wall in cells
  int gap = round(radius/cellSize);
  for (int i=0; i<width/cellSize; i++) {
    for (int j=0; j<height/cellSize; j++) {
      if (cells[i][j]) {
        if (sqrt((i-ii)*(i-ii)+(j-jj)*(j-jj)) <= gap)
          return true; // ii,jj is within the range of another wall
      }
    }
  }
  return false;
}

////////// HELPER FUNCTIONS

void drawBranch() {
  for (int[] pos: branch) {
    fill(branchColor);
    rect(pos[0]*cellSize, pos[1]*cellSize, cellSize, cellSize);
  }
}
