/*
lets test some procedual generation map algorithms!
TODO: map editor. export to txt file?
*/

// matrix of cells! true ones are walls, and false ones aren't
boolean[][] cells;
int cellSize = 20;

color wallColor = color(200,40,200);
color gapColor = color(50,230,255);

void setup() {
  size(940,700);
  noSmooth(); // I like my pixels thanks
  noStroke();
  background(50,230,255); // cyan whoo!
  cells = new boolean[width/cellSize][height/cellSize];
  
  drawCells();
}

void draw() {
  mapEdit();
  drawCells();
}

// MAP EDITOR

void mapEdit() {
  if (mousePressed) {
    // convert mouse position into cell index
    int i = int(map(mouseX, 0, width, 0, width/cellSize)); // use Map to avoid out of bound errors
    i = constrain(i, 0, width/cellSize-1); // prevents the possibility of xCellOver == width/cellSize (when mousex == width)
    int j = int(map(mouseY, 0, height, 0, height/cellSize));
    j = constrain(j, 0, height/cellSize-1);
    if (mouseButton == LEFT) {
      // add wall at mouse position
      cells[i][j] = true;
    } else if (mouseButton == RIGHT) {
      // remove wall at mouse position
      cells[i][j] = false;
    }
  }
  if (keyPressed) {
    if (key=='c' || key == 'C') { // Clear all
      for (int x=0; x<width/cellSize; x++) {
        for (int y=0; y<height/cellSize; y++) {
          cells[x][y] = false; // Save all to zero
        }
      }
    }
  }
}

// RANDOM CELLS

void randomCells() {
  // sets the values for the cells randomly
  for (int i=0; i<width/cellSize; i++) {
    for (int j=0; j<height/cellSize; j++) {
      float state = random(1);
      cells[i][j] = state < 0.5;
    }
  }
}

// GAME OF LIFE

void gameOfLife() {
  /*
  rules:
  live: 2 or 3 live neighbours -> live; else -> die;
  dead: 3 live neighbours -> live; else -> die;
  */
  delay(50);
  // for reading previous cell values while writing to cells
  boolean[][] cellBuffer = new boolean[width/cellSize][height/cellSize];
  for (int i=0; i<width/cellSize; i++) {
    for (int j=0; j<height/cellSize; j++) {
      cellBuffer[i][j] = cells[i][j];
    }
  }
  for (int i=0; i<width/cellSize; i++) {
    for (int j=0; j<height/cellSize; j++) {
      // find number of live neighbours
      int liveNeighbours = 0;
      for (int ii=i-1; ii<=i+1; ii++) {
        for (int jj=j-1; jj<=j+1; jj++) {
          if (!(ii==i && jj==j)) { // not self
            if (inRange(ii,0,width/cellSize)&&inRange(jj,0,height/cellSize)) { // valid index
              if (cellBuffer[ii][jj]) // live neighbour!
                liveNeighbours++;
            }
          }
        }
      }
      if (cellBuffer[i][j]) { // alive
        if (liveNeighbours < 2 || 3 < liveNeighbours)
          cells[i][j] = false; // dead now
      } else { // dead
        if (liveNeighbours == 3)
          cells[i][j] = true; // alive now!
      }
    }
  }
}

// HELPER FUNCTIONS

void drawCells() {
  for (int i=0; i<width/cellSize; i++) {
    for (int j=0; j<height/cellSize; j++) {
      if (cells[i][j])
        fill(wallColor);
      else
        fill(gapColor);
      rect(i*cellSize, j*cellSize, cellSize, cellSize);
    }
  }
}

boolean inRange(int value, int lower, int upper) {
  // is value in the range of lower and upper?
  return lower <= value && value < upper;
}
